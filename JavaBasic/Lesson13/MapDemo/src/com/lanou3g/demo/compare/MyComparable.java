package com.lanou3g.demo.compare;

import com.lanou3g.demo.Person;

public interface MyComparable<T> {
    int compareTo(T other);
}
