package com.lanou3g.demo.iter;

public interface Iterator<T> {

    T next();

    void add(T obj);

}
