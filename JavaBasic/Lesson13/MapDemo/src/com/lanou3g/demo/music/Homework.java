package com.lanou3g.demo.music;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Homework {

    public static void main(String[] args) {

        List<Album> albums = getAlbums();
        // 写到文件里


        System.out.println("------------");

        FileWriter writer = null;
        try {
            writer = new FileWriter("Music.txt");
            for(Album album: albums){
                writer.write(album.getName());
                List<Music> musics = album.getMusicList();
                for(Music m: musics){
                    String s = "\t歌曲名称: " + m.getMusicName() + ", 歌曲时长: " + m.getMusicDuration() + "\n";
                    writer.write(s);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }



        System.out.println("-------");
        // 第1张专辑中的第2首歌的名字
        Album album = albums.get(1);
        List<Music> musicList = album.getMusicList();
        Music music = musicList.get(2);
        System.out.println(music.getMusicName());
        System.out.println("------");
        String name = albums.get(1)
                        .getMusicList()
                        .get(2)
                        .getMusicName();



    }

    private static List<Album> getAlbums() {
        List<Album> albums = new ArrayList<>();
        Album jay = new Album();
        albums.add(jay);
        jay.setName("Jay");
        List<Music> jayMusic = new ArrayList<>();
        jay.setMusicList(jayMusic);
        Music dn = new Music("斗牛", "100");
        jayMusic.add(dn);
        jayMusic.add(new Music("反方向的钟", "200"));
        jayMusic.add(new Music("黑色幽默", "300"));
        jayMusic.add(new Music("娘子", "220"));

        Album bdkj = new Album();
        albums.add(bdkj);
        bdkj.setName("八度空间");
        List<Music> bdkjMusic = new ArrayList<>();
        bdkj.setMusicList(bdkjMusic);

        bdkjMusic.add(new Music("半岛铁盒", "129"));
        bdkjMusic.add(new Music("暗号", "328"));
        bdkjMusic.add(new Music("龙拳", "388"));
        bdkjMusic.add(new Music("半兽人", "222"));
        return albums;
    }

}
