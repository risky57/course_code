package com.lanou3g.demo.music;

public class Music {
    private String musicName;
    private String musicDuration;

    public Music(String musicName, String musicDuration) {
        this.musicName = musicName;
        this.musicDuration = musicDuration;
    }

    public String getMusicName() {
        return musicName;
    }

    public void setMusicName(String musicName) {
        this.musicName = musicName;
    }

    public String getMusicDuration() {
        return musicDuration;
    }

    public void setMusicDuration(String musicDuration) {
        this.musicDuration = musicDuration;
    }
}
