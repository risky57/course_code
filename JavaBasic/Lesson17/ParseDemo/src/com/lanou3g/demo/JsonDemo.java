package com.lanou3g.demo;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lanou3g.demo.entity.Album;
import com.lanou3g.demo.entity.Music;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.List;

public class JsonDemo {
    public static void main(String[] args) throws FileNotFoundException {
        // JSON 格式
        /*
        JSON 格式的数据由两种数据类型组成:
        第一种: {} 括起来的部分叫做对象
        第二种: [] 括起来的部分叫做数组或集合

        对象中的数据都是以键值对(key-value)的形式保存的
        key 一定是字符串, value可以是数字/字符串/boolean/对象/null

        解析JSON数据常用三种框架: 用哪个都行
        1. jackson
        2. gson  Google
        3. fastJson   阿里巴巴
         */

        // 解析步骤
        // 1. 创建gson对象
        Gson gson = new Gson();
        // 2. 判断json数据的最外层的数据类型, 确定解析的结果数据类型
        Type t = new TypeToken<List<Album>>(){}.getType();
        // 3. 开始解析
        Reader r = new FileReader("album-list.json");
        List<Album> albumList = gson.fromJson(r, t);

        for (Album album : albumList) {
            System.out.println(album.getAlbumName());
            List<Music> musicList = album.getMusicList();
            for (Music music : musicList) {
                System.out.println(music.getMusicName());
                System.out.println(music.getDuration());
            }
        }

        // 解析最外层是{}的情况
        // 1. 创建Gson对象
        // 2. 分析结果类型
        // 3. 解析
        Reader reader = new FileReader("album.json");
        Album album = gson.fromJson(reader, Album.class);
        System.out.println("--------");
        System.out.println(album.getAlbumName());
        for (Music music : album.getMusicList()) {
            System.out.println(music);
        }

        // 将对象序列化成字符串
        String json = gson.toJson(album);
        System.out.println(json);


    }
}
