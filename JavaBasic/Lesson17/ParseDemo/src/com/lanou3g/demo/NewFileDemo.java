package com.lanou3g.demo;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;

public class NewFileDemo {
    public static void main(String[] args) throws DocumentException, IOException {

        SAXReader r = new SAXReader();
        Document doc = r.read(new File("album-list.xml"));
        Element rootElement = doc.getRootElement();
        Iterator<Element> albumIterator = rootElement.elementIterator();
        while (albumIterator.hasNext()){
            Element album = albumIterator.next();
            Iterator<Element> musicIterator = album.elementIterator();
            while (musicIterator.hasNext()){
                Element music = musicIterator.next();
                Element person = music.addElement("person");
                person.setText("周杰伦");
            }
        }

        Writer w = new FileWriter("new-album-list.xml");
        doc.write(w);
        w.close();


    }
}
