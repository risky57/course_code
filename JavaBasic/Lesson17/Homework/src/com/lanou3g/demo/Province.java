package com.lanou3g.demo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Province {

    @SerializedName("省")
    private String provName;
    @SerializedName("市")
    private List<City> cities;

    public String getProvName() {
        return provName;
    }

    public void setProvName(String provName) {
        this.provName = provName;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}
