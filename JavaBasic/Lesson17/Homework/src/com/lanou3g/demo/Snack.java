package com.lanou3g.demo;

public class Snack {

    /**
     * id : 10001
     * name : 甜辣鸭脖
     * price : 12.9
     * quantity : 10
     * unit : 袋
     */

    private int id;
    private String name;
    private double price;
    private int quantity;
    private String unit;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
