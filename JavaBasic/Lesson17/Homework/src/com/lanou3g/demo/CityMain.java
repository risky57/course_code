package com.lanou3g.demo;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.List;

public class CityMain {
    public static void main(String[] args) throws FileNotFoundException {

        Reader r = new FileReader("ChineseCity.txt");
        Data d = new Gson().fromJson(r, Data.class);

        List<Province> provinces = d.getProvinces();
        for (Province province : provinces) {
            System.out.println(province.getProvName());
            List<City> cities = province.getCities();
            for (City city : cities) {
                System.out.println("\t" + city.getCityName() + " " + city.getCode());
            }
        }


    }
}
