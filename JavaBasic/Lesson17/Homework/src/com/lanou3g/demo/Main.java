package com.lanou3g.demo;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import javax.swing.text.Document;
import java.io.*;

public class Main {
    public static void main(String[] args) throws Exception {
        FileReader reader = new FileReader("snack-list.txt");
        BufferedReader br = new BufferedReader(reader);
        String line = null;
        org.dom4j.Document doc = DocumentHelper.createDocument();
        Element snackList = doc.addElement("snackList");
        while ((line=br.readLine())!=null){
            Element snack = snackList.addElement("snack");
            Element id = snack.addElement("id");
            Element name = snack.addElement("name");
            Element price = snack.addElement("price");
            Element quantity = snack.addElement("quantity");
            Element unit = snack.addElement("unit");

            String[] split = line.split(",");
            id.setText(split[0]);
            name.setText(split[1]);
            price.setText(split[2]);
            quantity.setText(split[3]);
            unit.setText(split[4]);
        }
        Writer w = new FileWriter("output.xml");
        doc.write(w);
        w.close();

    }
}
