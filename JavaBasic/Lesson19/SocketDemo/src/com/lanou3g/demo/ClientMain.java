package com.lanou3g.demo;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ClientMain {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("172.16.16.213", 7890);
        OutputStream out = socket.getOutputStream();
        PrintStream p = new PrintStream(out);

        InputStream in = socket.getInputStream();
        InputStreamReader r = new InputStreamReader(in);
        BufferedReader br = new BufferedReader(r);

        Scanner sc = new Scanner(System.in);

        while (true){
            System.out.println("请输入:");
            String line = sc.nextLine();
            p.println(line);
            p.flush();
            if (line.equals("bye")){
                break;
            }
            String word = br.readLine();
            System.out.println("服务端说: " + word);
        }

        // 关所有流


    }
}
