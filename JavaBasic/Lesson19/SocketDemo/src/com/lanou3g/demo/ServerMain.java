package com.lanou3g.demo;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;

public class ServerMain {
    public static void main(String[] args) throws IOException {
        // Socket 套接字
        // 在java中Socket类可以直接操作TCP协议

        // 创建服务端的Socket, 设置需要监听的端口号
        ServerSocket server = new ServerSocket(7890);
        // 如果监听到有客户端连接端口了,
        // 就可以获取到连接过来的socket对象.
        // 该方法是一个阻塞的方法
        while (true) {
            System.out.println("服务端已启动, 开始监听7890端口");
            System.out.println("等待客户端连接...");
            Socket socket = server.accept();
//            new Thread(new SocketRunnable(socket)).start();

            Executors.newCachedThreadPool()
                    .execute(new SocketRunnable(socket));

        }

    }
}
