package com.lanou3g.demo;

import java.io.IOException;
import java.net.*;

public class UdpClientDemo {
    public static void main(String[] args) throws IOException, InterruptedException {

        DatagramSocket socket = new DatagramSocket();

        byte[] buf = "UDP, 你好啊".getBytes();
        // 本机的IP地址
//        InetAddress addr = InetAddress.getLocalHost();
        InetAddress addr = InetAddress.getByName("127.0.0.1");
        // 构建数据包
        DatagramPacket dp = new DatagramPacket(buf, buf.length, addr, 6789);

        while (true) {
            socket.send(dp);
            Thread.sleep(1000);
        }


    }
}
