public class Main {

    public static void main(String[] args) {
        // 运算符
        // 一. 算数运算符
        // + - * / %
        // 除法 / : 对两个数做除法运算, 结果取商
        // 求余 % :                , 结果取余数
        System.out.println(3800 / 1000);
        System.out.println(3800 % 1000);
        // 两个不同类型的变量进行运算, 那么结果是什么类型?
        // 结果是精度更高的类型, 也就是表示范围更大的类型
//        byte b = 10;
//        b = b + 20;
//        float f = 3.6F;
//        f = f + 2.8;
        // 如果 '+' 对字符串做运算, 那么就代表连接字符串的意思
        String s = "hello";
        System.out.println(s + (1 + 2));
        System.out.println(1 + 2 + s);
        // ++  语法 n++ / ++n 对变量n自身+1
        // --  语法 n-- / --n 对变量n自身-1

        int i = 10;
        int r = i++ * ++i - i++ + ++i;
        //  10 * 12 - 12 + 14
        System.out.println(r);
        // 122
        // 复合运算符 +=  -=  *=  /=  %=
        // 复合运算符会自动进行类型的转换
//        int a = 5;
//        a += 3; // a = a + 3
//        byte b = 5;
//        b += 10;
        float f = 3.8F;
        f += 2.6;

        char x = 'A';
        x += 32;
        System.out.println(x);
        // 交换两个变量的值, 不定义第三个变量, 如何做
        int a = 12120010;
        int b = 110230034;
        a = a + b;
        b = a - b;
        a = a - b;
        // 有什么坏处? 数太大的话有可能会超出范围, 结果不准确


    }
}
