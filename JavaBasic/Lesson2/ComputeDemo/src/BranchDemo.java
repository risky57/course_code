public class BranchDemo {
    public static void main(String[] args) {
        // 分支结构
        /*
        if语句
        语法:
        if(条件){
            代码块;
        }
        条件: 是返回值为boolean型的表达式或值
        代码块: 条件的返回值为true的情况下所执行的代码
         */
        // 练习: Java分数如果大于90, 那么输出成绩优秀
        int score = 30;
        if (score > 90) {
            System.out.println("成绩优秀");
        }
        if (score <= 90) {
            System.out.println("重修");
        }
        /*
        第二种if结构
        语法:
        if(条件){
            代码块1;
        } else {
            代码块2;
        }
        当条件满足的情况下执行代码块1,
        当条件不满足, 会执行代码块2.
         */
        int javaExamScore = 78;

        if (javaExamScore >= 60) {
            System.out.println("进入下个阶段");
        } else {
            System.out.println("重修");
        }

        //
        int score1 = 80;
        int score2 = 90;
        if (score1 > 85 && score2 > 80) {
            System.out.println("进入下阶段学习");
        } else {
            System.out.println("重新学习");
        }

        /*
        if第三种结构:
        if(条件1){
            代码块1;
        } else if(条件2){
            代码块2;
        } else if(条件3){
            代码块3;
        } ... else if(条件n){
            代码块n
        } else {
            代码块n + 1
        }
         */
        // 练习:
        // 如果分数 (90, 100] 一等奖学金 200元
        // 分数    (80, 90]  二等奖学金 奖励个妹子
        //         [60, 80] 三等奖学金  娃娃
        //  其他             四等奖学金  两脚
        int scoreA = 99;
        if (scoreA > 90) {
            System.out.println("一等奖学金:200元");
        } else if (scoreA > 80) {
            System.out.println("二等奖学金：妹子");
        } else if (scoreA >= 60) {
            System.out.println("三等奖学金：娃娃");
        } else {
            System.out.println("四等奖学金：两脚");
        }

        /*
        if第四种
        if嵌套
         */
        // 练习:
        // 百米跑步的成绩,
        // 如果是男的, 那么成绩需要小于11秒才进入决赛.
        // 如果是女的, 那么成绩需要小于12秒才进入决赛.
        char gender = 'F';
        float time = 11.8F;
        if (gender == 'M') {
            if (time < 11) {
                System.out.println("男子组决赛");
            }
        } else if (gender == 'F') {
            if (time < 12) {
                System.out.println("进入女子组决赛");
            }
        }

        String sex = "男";
        // 判断两个字符串是否相等
        // 注意: 判断两个字符串是否相等时不能使用 ==
        if (sex.equals("男")){

        }

        String s = "春天";
        switch (s){
            case "春天":
                System.out.println("温暖");
                break;
            case "夏天":
                System.out.println("火热");
                break;
            case "秋天":
                System.out.println("秋风扫落叶");
                break;
            case "冬天":
                System.out.println("冷酷无情");
                break;
        }

    }
}
