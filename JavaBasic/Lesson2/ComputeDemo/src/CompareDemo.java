public class CompareDemo {
    public static void main(String[] args) {
        // 二. 比较运算符
        // 比较两个值的大小关系,
        // 由比较运算符组成的表达式, 称之为比较表达式
        // 比较表达式的结果(返回值)是boolean类型,
        // 结果只能是true或者false
        // true: 真
        // false: 假
        // > >= < <= == !=
        int a = 5, b = 3, c = 5;
        System.out.println(a > b); // true
        System.out.println(a >= b);// true
        System.out.println(a < b); // false
        System.out.println(a <= b);// false
        System.out.println(a <= c);// true
        System.out.println(a == c);// true
        System.out.println(a != c);// false
        System.out.println(a != b);// true
        // 三目运算符/三元运算符
        // 变量 = 比较表达式 ? 值1 : 值2;
        // 如果表达式的结果为true, 那么就把值1赋值给变量,
        // 反之, 把值2赋值给变量
        // 练习1: 有两个变量a和b, 取出两个变量中较大的一个
        // 练习2: 有一个整数a, 计算该值的绝对值

        int A = 5, B = 3, max;
        max = A > B ? A : B;
        System.out.println(max);
        int D = -5;
        int abs = D > 0 ? D : -D;
        System.out.println(abs);

    }
}
