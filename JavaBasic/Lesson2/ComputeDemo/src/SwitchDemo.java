public class SwitchDemo {
    public static void main(String[] args) {
        // switch 分支结构
        /*
        语法:
        switch(表达式){
            case 值1:
                代码块1;
                break;
            ...
            case 值n:
                代码块n;
                break;
            default:
                代码块n + 1;
        }
        表达式: 表达式的结果必须可以自动转换为int型.
        执行的过程:
        表达式执行完毕后, 看返回值的结果, 结果是几那么
        就会执行该值所对应的case中的代码块.
        如果所有的case都没有对应的值, 那么就会执行default
        中的代码块.
        执行的过程中, 遇到break就会跳出switch结构.
        如果该case中没有break, 那么就会顺序执行下一个case,
        直到遇到break或switch结构结束为止.
         */
        int rank = 2;
        switch (rank){
            case 1:
                System.out.println("夏令营");
//                break;
            case 2:
                System.out.println("笔记本");
//                break;
            case 3:
                System.out.println("硬盘");
                break;
            default:
                System.out.println("没有奖励");
                break;
        }
    }
}
