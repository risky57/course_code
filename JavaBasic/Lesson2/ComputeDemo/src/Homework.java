import java.util.Scanner;

public class Homework {
    public static void main(String[] args) {
        // 1.
        System.out.println("**********");
        System.out.println("*** JAVA ***");
        System.out.println("**********");

        Scanner sc = new Scanner(System.in);

        // 2. 189
        System.out.println("请输入钱数:");
        int money = sc.nextInt();
        int count100 = money / 100;
        // 除去100之后剩下的钱
        money = money % 100;
        int count50 = money / 50;
        //
        money %= 50;
        int count20 = money / 20;
        money %= 20;
        int count10 = money / 10;
        money %= 10;
        int count5 = money / 5;
        money %= 5;
        int count1 = money / 1;
        System.out.println("需要100元 " + count100 + " 张");
        System.out.println("需要50元 " + count50 + " 张");
        System.out.println("需要20元 " + count20 + " 张");
        System.out.println("需要10元 " + count10 + " 张");
        System.out.println("需要5元 " + count5 + " 张");
        System.out.println("需要1元 " + count1 + " 张");

        // 3
        // 十进制转换为二进制
        // 337
        //  512 256 128 64 32 16 8 4 2 1
        //   0   1   0   1  0  1 0 0 0 1
        System.out.println("判断奇数偶数, 请输入整数: ");
        int n = sc.nextInt();
        if (n % 2 == 0){
            System.out.println("偶数");
        } else {
            System.out.println("奇数");
        }
        if (n >> 1 << 1 == n){
            System.out.println("偶数");
        } else {
            System.out.println("奇数");
        }
        // 4. 50公斤 0.15   超出的每公斤加收0.1
        System.out.println("请输入行李重量:");
        int weight = sc.nextInt();
        float price;
        if (weight <= 50){
            price = weight * 0.15F;
        } else {
            price = 50 * 0.15F + (weight - 50) * 0.25F;
        }
        System.out.println(weight + "公斤的行李应该收取" + price + "元");

        // 5.
        System.out.println("请输入年份: ");
        int year = sc.nextInt();
        if (year % 100 != 0 && year % 4 == 0){
            System.out.println("闰年");
        } else if (year % 400 == 0){
            System.out.println("闰年");
        } else {
            System.out.println("平年");
        }
        // 6.
        System.out.println("请输入三个整数: ");
        int a = sc.nextInt(), b = sc.nextInt(), c = sc.nextInt();
        int max = a > b ? a : b;
        max = max > c ? max : c;
        System.out.println("三个数中最大的是: " + max);
        // 7.交换
        int i = 14, j = 20; //  k
        // 0000 1110
        // 0001 0100
        // 0001 1010
        i = i ^ j;
        j = i ^ j;
        i = i ^ j;
    }
}
