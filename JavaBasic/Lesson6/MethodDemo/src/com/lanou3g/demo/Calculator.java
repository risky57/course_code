package com.lanou3g.demo;

/**
 * 计算器类
 */
public class Calculator {

    // 如果调用某个方法之后需要有一个执行的结果,
    // 那么该结果就叫做返回值,
    // 在方法的返回值类型位置明确指出该类型
    // 如果某个方法有返回值类型, 那么必须使用return关键字
    // 将方法的结果返回
    /**
     * 两个数的和
     * @param x
     * @param y
     */
    public int add(int x, int y){
//        int sum = x + y;
        return x + y;
    }

    public int abs(int n){
        if (n > 0) {
            return n;
        } else {
            return -n;
        }
        // return n > 0 ? n : -n;
    }

    // 计算两个整数绝对值的和
    public int ageAdd(int x, int y){
//        int x1 = abs(x);
//        int y1 = abs(y);
//        return add(x1, y1);
        return add(abs(x), this.abs(y));
    }
}
