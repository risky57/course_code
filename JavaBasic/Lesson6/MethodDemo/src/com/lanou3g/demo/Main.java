package com.lanou3g.demo;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        Calculator calculator = new Calculator();
        // 调用某个方法的时候,
        // 必须按照方法声明时的参数规则填写参数

        int a = 3, b = 5;
        // 将某个方法的返回值赋值给一个变量
        int sum = calculator.add(a, b);
        System.out.println(a + "与" + b + "的和为: " + sum);

        // 在调用的时候传递的是什么参数,
        // 那么在这次的调用中, 形参的值就是什么
        int sum1 = calculator.add(6, 10);

        Person p = new Person();
        p.setName("孙悟空");
//        p.age = 5000;
        p.setAge(-200);
        System.out.println(p.getName());

        p.eat("米饭", 3);

//        "".indexOf()
        // 1. 10个类, 3条属性, getter&setter,
            // 再定义一个方法, 用来打印所有属性的值
        // 2. 有一个字符串, 打印出在字符串中出现的某个字符的下标
        String s = "123";


        // alt + 回车
        int i = s.indexOf("1");
        int n = 3;

        String str = "A1B2C3D4F5A6B7C8D9E0AaBbCcDdEeFf";
        int index = -1;
        do {
            index = str.indexOf('A', index + 1);
            if (index != -1){
                System.out.println(index);
            }
        }while (index != -1);

        String numStr = "19287";
        int result = 0;
        int r = 1;
        for (i = numStr.length() - 1; i >= 0; i--){
            char ch = numStr.charAt(i);
            int num = ch - 48;
            result += num * r;
            r *= 10;
        }
        System.out.println(result);




    }
}
