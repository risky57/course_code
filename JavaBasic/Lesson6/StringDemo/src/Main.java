import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        // 字符串
        String str = "    让1教2育3  回4归  5本6质7...  归本质...本质...质   ";
        // 字符串是通过封装char而得到的
        // 1. 获取字符串的长度
        int length = str.length();
        // 2. 获取字符串中第index位置的字符
        char c = str.charAt(3);
        System.out.println(c);
        // 3. 获取某个字符在字符串中第一次出现的下标
        int index = str.indexOf('本');
        System.out.println(index);// 输出5
        // 方法的重载:
        // 在同一个类中, 存在多个方法, 这几个方法满足下述的规则:
        // 1) 方法名相同
        // 2) 参数列表不一样(参数类型/个数/顺序)
        // 实际上的原则: 当调用的时候可以明确区分调用的是哪一个即可
        // 查第二个 '本' 在字符串中的位置
        int index2 = str.indexOf('本', index + 1);
        System.out.println(index2);
        // 查字符串中某个子串的位置
        int index3 = str.indexOf("本育");
        System.out.println(index3);// 在str中没有该子串, 输出-1
        int index4 = str.indexOf("本育", 3);

        // 4. 判断字符串中是否包含某个子串
        boolean isContains = str.contains("本质");
        System.out.println(isContains);

        // 5. 判断某个字符串是不是以xxx开始的
        boolean isStartWith = str.startsWith("让");

        // 6. 判断某个字符串是不是以xxx结束的
        boolean isEndWith = str.endsWith("太平");

        // 7. 检索某一个字符在字符串中最后出现的位置
//        str.lastIndexOf()
        // 8. 将字符串中的某个字符都替换为新的字符
        //    原字符串对象没有变化, 该方法会返回一个新的字符串对象
        String s = str.replace('本', 'A');
        System.out.println(s);
        String s1 = str.replace("本质", "AB");
        // 把满足正则表达式的子串都替换为某个字符串
        String s2 = str.replaceAll("\\d", "ABC");
        System.out.println(s2);

        // 9. 判断某个字符串是否是空的
        String str1; // 只声明了, 没有值, 不能用
        String str2 = null; // 空指针异常
        String str3 = ""; // true
        String str4 = " "; // false
        boolean empty = str3.isEmpty();

        // 10. 字符串的截取[beginIndex, endIndex)
        String substring = str.substring(6, 10);
        System.out.println(substring);

        // 11. 把字符串中的英文都转换为小写字母
//        String s = str.toLowerCase()
        // 都转换为大写字母
//        String s = str.toUpperCase();

        // 12. 去掉字符串的首尾所有空格
        String trim = str.trim();
        System.out.println(trim);

        // 13. 把字符串转换为字节数组
        byte[] bytes = str.getBytes();

        // 14. 把字符串转换为字符数组
        char[] chars = str.toCharArray();

        // 15. 根据正则表达式把字符串分割为字符串的数组
        String[] strings = str.split("\\.");
        System.out.println(Arrays.toString(strings));

        // 16. 判断两个字符串是否一样
        boolean isEquals = str.equals("aaaa");

        String s11 = new String("ABCDE");
        String s12 = new String("ABCDE");
        System.out.println(s11 == s12);
        System.out.println(s11.equals(s12));

        // 17. 忽略大小写判断两个字符串是否一样
        // 比如 判断验证码
        String s21 = "hello";
        String s22 = "Hello";
        boolean b = s21.equalsIgnoreCase(s22);
        System.out.println(b);


        String a = "12345";
//        Main m = new Main();
//        int result = m.parseInt(a);
        int result = Main.parseInt(a);

        System.out.println(result);

        String s4 = "A1A2A3A4A5lsdlAlsjdfAlksjdfA";
        char ch = 'A';
//        for (int i = 0; i < s4.length(); i++) {
//            if ('A' == s4.charAt(i)){
//
//            }
//        }

        int x = 'A';
        char y = 'A';


        int idx = -1;
        do {
            idx = s4.indexOf(ch, idx + 1);
            if (idx == -1) break;
            System.out.println(idx);
        } while (true);


    }

    public static int parseInt(String str){
        int result = 0;
        int r = 1;
        for (int i = str.length() - 1; i >= 0 ; i--) {
            char ch = str.charAt(i);
            int n = ch - 48;
            result += n * r;
            r *= 10;
        }
        return result;
    }
}
