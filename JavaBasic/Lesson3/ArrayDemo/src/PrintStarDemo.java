import java.util.ArrayList;

public class PrintStarDemo {
    public static void main(String[] args) {

        int n = 4;
        /*
         *
         ***
         *****
         *******
         *********
         */

        /*
        1 *
        2 ***
        3 *****
        4 *******
        5 *********
         */
        /*

        1 ****  4
        2 ***   3
        3 **    2
        4 *     1
        5       0
         */
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n - i; j++) {
                System.out.print(" ");
            }
            for (int j = 1; j <= 2 * i - 1; j++) {
                if (j == 1 || j == 2 * i - 1){
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

        // n = 5  b = 9
        // n = 7  b = 13

        /*
        1 *******  11
        2 *****  9
        3 ***    7
        4 *      5

        ax + b = y
        y = -2x + 9

        a + b = 11
        2a + b = 9

        a = -2
        b = 9


         */
        for (int i = 1; i <= n - 1; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(" ");
            }
            for (int j = 1; j <= 2 * (n - i) - 1; j++) {
                if (j == 1 || j == 2 * (n - i) - 1){
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

//        for (int i = 1; i <= n - 1 ; i++) {
//            for (int j = 1; j <= i ; j++) {
//                System.out.print(" ");
//            }
//            System.out.println();
//        }

        /*
        1 *
        2 **
        3 ***
        4 ****

         */

        int[][] array = new int[3][4];
        // 二维数组: 可以看做每个元素都是一个一维数组的一维数组
        array[0][0] = 5;

        System.out.println(array[0]);
        array[0] = new int[5];
//        System.out.println(array[1][4]);
        /*
        _ _ _ _ _
        _ _ _ _
        _ _ _ _
         */
        System.out.println(array.length);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.println(array[i][j]);
            }
        }

    }
}
