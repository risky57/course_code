import java.util.Arrays;

public class Ex {
    public static void main(String[] args) {
        // 有一个任意长度的数组, 将数组中的元素倒序
        int[] array = {1, 3, 2, 8, 9, 7, 5, 6};
        /*
        x +y  = array.length - 1
        0  8
        1  7
        2  6
        3  5

         */
//        int[] array = {4, 6, 5, 7, 9, 8, 2, 3, 1};
        for (int i = 0; i < array.length / 2; i++) {
//            System.out.println(i + " " + (array.length - i - 1));
            int index1 = i;
            int index2 = array.length - i - 1;
            int temp = array[index1];
            array[index1] = array[index2];
            array[index2] = temp;
        }
        System.out.println(Arrays.toString(array));

        // 将数组中的最大数移动到数组的末尾
        int[] arr = {1, 3, 2, 8, 9, 7, 5, 6};
//        for (int i = 0; i < arr.length - 1; i++) {
//            if (arr[i] > arr[i + 1]){
//                int temp = arr[i];
//                arr[i] = arr[i + 1];
//                arr[i + 1] = temp;
//            }
//        }
//        System.out.println(Arrays.toString(arr));

        //

        // 最大数
        int max = arr[0];
        // 最大数所在的下标
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= max){
                max = arr[i];
                index = i;
            }
        }
        int temp = arr[index];
        arr[index] = arr[arr.length - 1];
        arr[arr.length - 1] = temp;
        System.out.println(Arrays.toString(arr));


    }
}
