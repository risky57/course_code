import java.lang.reflect.Array;
import java.util.Arrays;

public class Homework {
    public static void main(String[] args) {
        // 1.
        int money = 1000000000;
        int day = 0;
        while (money > 0){
            money /= 2;
            day++;
        }
        System.out.println("可以花" + day + "天");
        // 2.
        float paper = 0.08F;
        float mountain = 8848130F;
        int times = 0;
        while (paper < mountain){
            paper *= 2;
            times++;
        }
        System.out.println("折叠" + times + "次");

        // 3. 水仙花数
        for (int i = 100; i < 1000; i++) {
            int a = i / 100;
            int b = i % 100 / 10;
            int c = i % 10;
            if (a * a * a + b * b * b + c * c * c == i){
                System.out.println(i);
            }
        }
        // 4.
        int[] array = {50, 88, 66, 32, 77};
        int n = 99;
        // 对原数组排序
        Arrays.sort(array);
        int[] newArr = new int[array.length + 1];
        boolean flag = true;
        for (int i = 0; i < array.length; i++){
            if (array[i] < n){
                newArr[i] = array[i];
            } else {
                if (flag){
                    newArr[i] = n;
                    flag = false;
                }
                newArr[i + 1] = array[i];
            }
        }
        if (n >= array[array.length - 1]){
            newArr[newArr.length - 1] = n;
        }
        System.out.println(Arrays.toString(newArr));
        // 4.
//        Arrays.sort(array);
//        int[] newArray = new int[array.length + 1];
//        newArray[newArray.length - 1] = n;
//        Arrays.sort(newArray);
        // 5. break
        // 在循环过程中, 如果遇到break, 那么会直接终止循环
        // 找到新值n应该插入的位置
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] >= n){
                index = i;
                break;
            }
        }

        // 处理n最大的时候边界情况
        if (n >= array[array.length - 1]){
            index = array.length;
        }
//
//        for (int i = 0; i < newArr.length; i++) {
//            if (i < index){
//                newArr[i] = array[i];
//            } else if (i == index){
//                newArr[i] = n;
//            } else {
//                newArr[i] = array[i - 1];
//            }
//        }
//        System.out.println(Arrays.toString(newArr));

        //
        for (int i = 0; i < array.length; i++) {
            newArr[i] = array[i];
        }

        for (int i = newArr.length - 1; i > index; i--) {
            newArr[i] = newArr[i - 1];
        }
        newArr[index] = n;
        System.out.println(Arrays.toString(newArr));

        // 5的阶乘
        // 1*2*3*4*5*6*7*8*9*10竟然等于10!
        // 阶乘定义: f(n) = n * f(n - 1)

        int result = 1;
        for (int i = 1; i <= 5; i++) {
            result *= i;
        }

        // 斐波那契数列
        //  1 1 2 3 5 8 13 21 34 55 89 ...
        // f(n) = f(n - 2) + f(n - 1)
        // f(0) = 1; f(1) = 1;
        int lastOne = 1;
        int lastTwo = 1;
        System.out.print(lastOne + " ");
        System.out.print(lastTwo + " ");
        for (int i = 0; i < 20; i++) {
            int num = lastOne + lastTwo;
            lastOne = lastTwo;
            lastTwo = num;
            System.out.print(num + " ");
        }



    }
}
