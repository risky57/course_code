public class Main {

    public static void main(String[] args) {
        // 数组
        // 数组是在内存中划分一串连续的内存空间。
        // 创建数组时, 必须指定数据类型. 数据类型指定好之后就是不可变的
        // 数组的长度也必须指定, 指定后也不可变
        // 数组中存放的数据叫做元素
        // 数组中每个元素的位置叫做下标, 注意: 从0开始
        // 数组声明的方式
        // 1.直接指定了数组的类型/长度/元素的值
        int[] array1 = {9, 1, 3, 6, 2, 7};
        int array2 [] = {9, 1, 3, 6, 2, 7}; // 不建议使用
        // 2.指定了数组的类型和长度
        int[] array3 = new int[10];
        System.out.println(array3[0]);
        // 引用数据类型的默认值为 null
        String s = "";
        String[] arr = new String[20];
        // 3. 先声明, 再赋值
        float[] array4;
        array4 = new float[8];
        // 这种的不可以
//        int[] array5;
//        array5 = {1, 2, 3};

        boolean[] b = new boolean[3];
        b[0] = true;
        b[1] = false;
        b[2] = true;


        // 根据下标获取数组中的某个元素
        System.out.println(array1[3]);
        // 为数组中的下标为2的元素赋值
        array1[2] = 200;
        // 获取数组的长度
        // 调用数组array1的length属性
        // sc.nextInt() 方法
        System.out.println(array1.length);
        // 第二个异常: 数组越界异常
//        System.out.println(array1[array1.length]);
        // 获取数组中的最后一个元素
        System.out.println(array1[array1.length - 1]);

        int[] a = new int[3];
        a = new int[10];

    }
}
