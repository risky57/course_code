import java.util.Arrays;

public class ForDemo {
    public static void main(String[] args) {
        // break 遇到break, 循环会直接停止
        // continue 遇到continue, 会停止当前的循环,
        // 直接进行下次循环
        for (int i = 0; i < 100; i++) {
//            System.out.println(i);
        }
        // 循环嵌套
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + "x" + i + "=" + i * j + "\t");
            }
            System.out.println();
        }
        // 判断某个数是不是质数
        int n = 5;
//        int i;
//        for (i = 2; i <= n / 2; i++) {
//            if (n % i == 0){
//                break;
//            }
//        }
//        if (i >= n / 2){
//            System.out.println("质数");
//        } else {
//            System.out.println("合数");
//        }
        // 质因数分解
        // 120 = 2 * 2 * 2 * 3 * 5
        System.out.println("质因数分解: ");
        n = 7800;

        for (int j = 2; n > 1; j++) {
            if (n % j == 0) {
                n = n / j;
                System.out.print(j + " ");
                j = 1;
            }
        }
        // 100块钱买100只鸡
        System.out.println();
        int a = 20, b = 33, c = 300;
        for (int x = 0; x <= a; x++) {
            for (int y = 0; y <= b; y++) {
                for (int z = 0; z <= c; z += 3) {
                    if (x + y + z == 100 && 5 * x + 3 * y + z / 3 == 100) {
                        System.out.printf("公鸡: %s只, 母鸡: %s只, 雏鸡: %s只\n", x, y, z);
                    }
                }
            }
        }
        // 冒泡排序

        int[] arr = {1, 6, 3, 5, 8, 7, 2, 4};
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
        int x = 0;
        int[] array = {1, 3, 4, 2, 1, 2, 4, 5, 5, 8, 9, 9, 8};
        for (int i = 0; i < array.length; i++) {
            x ^= array[i];
        }
        System.out.println(x);
    }
}
