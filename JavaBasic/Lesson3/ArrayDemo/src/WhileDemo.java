public class WhileDemo {
    public static void main(String[] args) {
        // 1. while 循环
        /*
        语法:
        while(循环条件){
            循环体;
        }
        执行过程:
        先判断循环条件, 如果结果为true, 那么就会执行循环体部分代码;
        当循环体执行完毕后, 会再次判断循环条件, 如果为true, 继续执行
        循环体的代码. 重复上述过程, 直到某次循环的条件为false, 那么
        就会跳出循环
         */
        // 计算 1 + 2 + 3 +...+ 100
        // 变量的作用域: 变量生效的范围
        // 从变量声明开始, 一直到变量声明时所在的{}的末尾
        // 在同一个作用域, 同一个变量不能声明两次
        int i = 0;
        int sum = 0;
        while (i < 100) {
            i++;
            if (i % 2 == 0) {
                sum = sum + i;
            }
        }
        System.out.println(sum);

        int[] arr = {4, 3, 7, 1, 10, 9, 8, 2, 6};
        // 获取数组中的最大值/最小值/平均值
        // 遍历
        int max = arr[0];
        int min = arr[0];
        float avg;
        sum = 0;
        i = 0;
        while (i < arr.length) {
            max = max > arr[i] ? max : arr[i];
            min = min < arr[i] ? min : arr[i];
            sum += arr[i];
            i++;
        }
        avg = sum * 1.0F / arr.length;
        System.out.println("最大值: " + max);
        System.out.println("最小值: " + min);
        System.out.println("平均值: " + avg);

        // 2. do...while
        /*
        语法:
        do {
            循环体;
        } while(循环条件);
        执行过程: 先执行一遍循环体, 然后进行条件的判断.
        如果条件结果为true, 则再次执行循环体.
        至少会执行一遍循环体.
         */

        // 3. for循环
        /*
        语法:
        for(循环变量初始化1; 循环条件2; 循环变量自增3) {
            循环体4;
        }
        执行过程:
        执行一次1, 判断条件2, 如果结果为true, 则执行4;
        4执行完毕会执行3, 然后再执行条件2, 开始循环.
        1 -> 2 -> 4 -> 3 -> 2 -> 4 -> 3 ...
         */
        sum = 0;
        for (int j = 1; j <= 100; j++) {
            sum += j;
        }

        sum = 0;
        max = arr[0];
        min = arr[0];
        for (int j = 0; j < arr.length; j++) {
            max = max > arr[j] ? max : arr[j];
            min = min < arr[j] ? min : arr[j];
            sum += arr[j];
        }

        max = max > arr[0] ? max : arr[0];
        max = max > arr[1] ? max : arr[1];
        max = max > arr[2] ? max : arr[2];
        max = max > arr[3] ? max : arr[3];
        max = max > arr[4] ? max : arr[4];
        max = max > arr[5] ? max : arr[5];
        max = max > arr[6] ? max : arr[6];
        max = max > arr[7] ? max : arr[7];
        max = max > arr[8] ? max : arr[8];
        System.out.println(max);

        // 怪物的血量
        int health = 100;
        // 攻击力
        int attack = 13;

        int times = 0;
        while (health > 0) {
            times++;
            if (times % 5 == 0){
                health -= attack * 2;
            } else {
                health -= attack;
            }
        }
        System.out.println(times);

        health = 100;
        times = 0;
        do {
            times++;
            if (times % 5 == 0){
                health -= attack * 2;
            } else {
                health -= attack;
            }
        } while (health > 0);
        System.out.println(times);

        health = 100;
        int j;
        for(j = 0; health > 0; j++){
            health -= attack;
        }
        System.out.println(j);

        for(j = 0; j < 100; j++){
        }
        System.out.println(j);
        // 0
        // 8
        // 99
        // 100

        System.out.println(3.5F % 2);
    }
}
