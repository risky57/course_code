package com.lanou3g.demo.shop.equip;

import com.lanou3g.demo.shop.entity.Goods;

import java.util.ArrayList;
import java.util.List;

public abstract class GoodsContainer {

    private String name;
    private List<GoodsCell> cells;

    public GoodsContainer() {
        cells = new ArrayList<>();
    }

    public void addGoods(Goods goods, int quantity){
        cells.add(new GoodsCell(goods.getId(), goods, quantity));
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    protected List<GoodsCell> getCells() {
        return cells;
    }

    public class GoodsCell{
        private int id;
        private Goods goods;
        private int quantity;

        public GoodsCell(int id, Goods goods, int quantity) {
            this.id = id;
            this.goods = goods;
            this.quantity = quantity;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Goods getGoods() {
            return goods;
        }

        public void setGoods(Goods goods) {
            this.goods = goods;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }
    }
}
