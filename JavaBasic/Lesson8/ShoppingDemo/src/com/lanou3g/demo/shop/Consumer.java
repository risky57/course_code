package com.lanou3g.demo.shop;

import com.lanou3g.demo.shop.base.Person;
import com.lanou3g.demo.shop.equip.GoodsContainer;
import com.lanou3g.demo.shop.equip.GoodsShelf;
import com.lanou3g.demo.shop.equip.ShoppingCart;

public class Consumer extends Person {

    private ShoppingCart shoppingCart;
    private GoodsShelf shelf;

    public void checkShelf(GoodsShelf shelf){
        this.shelf = shelf;
        System.out.printf("顾客`%s`开始查看`%s`货架上的商品\n", getName(), shelf.getName());
        shelf.print();
    }

    public void selectShoppingCart(ShoppingCart shoppingCart){
        this.shoppingCart = shoppingCart;
    }

    public boolean addGoods(int goodsId, int quantity){
        if (shoppingCart == null) {
            System.out.println("请先选择一个购物车");
            return false;
        }
        if (shelf == null) {
            System.out.println("请先选择一个货架");
            return false;
        }
        GoodsContainer.GoodsCell cell = shelf.remove(goodsId, quantity);
        if (cell == null) {
            System.out.println("该商品货物不足");
            return false;
        }
        this.shoppingCart.addGoods(cell.getGoods(), cell.getQuantity());
        return true;
    }
}
