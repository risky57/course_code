package com.lanou3g.demo.shop.person;

import com.lanou3g.demo.shop.base.Person;

public class Staff extends Person {

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
