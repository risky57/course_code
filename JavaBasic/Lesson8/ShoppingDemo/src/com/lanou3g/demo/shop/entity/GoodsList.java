package com.lanou3g.demo.shop.entity;

public class GoodsList {

    private String listName;
    private String listPath;

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getListPath() {
        return listPath;
    }

    public void setListPath(String listPath) {
        this.listPath = listPath;
    }
}
