package com.lanou3g.demo.shop.person;

import com.lanou3g.demo.shop.entity.GoodsList;
import com.lanou3g.demo.shop.equip.GoodsShelf;
import com.lanou3g.demo.shop.file.DefaultListReader;
import com.lanou3g.demo.shop.file.ListFileReader;

public class Worker extends Staff {

    private ListFileReader reader;

    public Worker() {
        reader = new DefaultListReader();
    }

    public boolean loadListToShelf(GoodsList list, GoodsShelf shelf){
        System.out.printf("装卸工`%s`读取`%s`清单, 并开始上货.\n", getName(), list.getListName());
        shelf.setName(list.getListName());
        boolean successful = reader.read(list, shelf);
        System.out.println("装卸工`" + getName() + (successful ? "`上货完成" : "`上货失败"));
        return successful;
    }

    public void setReader(ListFileReader reader) {
        this.reader = reader;
    }
}
