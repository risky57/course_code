package com.lanou3g.demo.shop.file;

import com.lanou3g.demo.shop.entity.GoodsList;
import com.lanou3g.demo.shop.equip.GoodsShelf;

public interface ListFileReader {

    boolean read(GoodsList list, GoodsShelf shelf);
}
