package com.lanou3g.demo.shop.file;

import com.lanou3g.demo.shop.entity.Goods;
import com.lanou3g.demo.shop.entity.GoodsList;
import com.lanou3g.demo.shop.equip.GoodsShelf;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class DefaultListReader implements ListFileReader {
    @Override
    public boolean read(GoodsList list, GoodsShelf shelf) {
        String path = list.getListPath();
        try (FileReader reader = new FileReader(path);
             BufferedReader br = new BufferedReader(reader)) {

            String line = null;
            while ((line = br.readLine()) != null) {
                String[] arr = line.split(",");
                Goods g = new Goods();
                g.setId(Integer.parseInt(arr[0]));
                g.setName(arr[1]);
                g.setPrice(Float.parseFloat(arr[2]));
                int quantity = Integer.parseInt(arr[3]);
                g.setUnit(arr[4]);
                shelf.addGoods(g, quantity);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
