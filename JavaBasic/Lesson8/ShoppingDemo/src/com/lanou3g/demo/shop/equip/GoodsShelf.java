package com.lanou3g.demo.shop.equip;

import com.lanou3g.demo.shop.entity.Goods;

import java.util.List;

public class GoodsShelf extends GoodsContainer {

    public void print() {
        List<GoodsCell> cells = getCells();
        System.out.println(getName() + "货架上的商品有:");
        System.out.println("编号\t\t商品名称\t\t单价\t\t\t\t数量");
        cells.forEach(c -> {
            Goods g = c.getGoods();
            System.out.printf("%s\t%s\t\t%s元/%s\t\t%s%s\n", c.getId(), g.getName(), g.getPrice(), g.getUnit(), c.getQuantity(), g.getUnit());
        });
    }

    public GoodsCell remove(int goodsId, int quantity) {
        List<GoodsCell> cells = getCells();
        GoodsCell cell = null;
        for (GoodsCell c : cells) {
            if (goodsId == c.getId()) {
                cell = c;
                break;
            }
        }
        if (cell != null && cell.getQuantity() >= quantity) {
            cell.setQuantity(cell.getQuantity() - quantity);
            return new GoodsCell(goodsId, cell.getGoods(), quantity);
        }
        return null;
    }


}
