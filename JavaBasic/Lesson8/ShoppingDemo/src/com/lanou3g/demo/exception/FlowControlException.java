package com.lanou3g.demo.exception;

public class FlowControlException extends Exception {

    public final int type;

    public FlowControlException(int type) {
        this.type = type;
    }

}
