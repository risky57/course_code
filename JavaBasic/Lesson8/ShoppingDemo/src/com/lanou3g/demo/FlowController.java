package com.lanou3g.demo;

import com.lanou3g.demo.exception.FlowControlException;

public interface FlowController {
    void start() throws FlowControlException;
}
