package com.lanou3g.demo;

import com.lanou3g.demo.exception.FlowControlException;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class FlowMain {
    public static void main(String[] args) {

        Map<Integer, FlowController> map = new HashMap<>();
        Scanner sc = new Scanner(System.in);

        while (true){
            int input = sc.nextInt();
            FlowController controller = map.get(input);
            if (controller != null) {
                try {
                    controller.start();
                } catch (FlowControlException e) {

                }
            } else {
                System.out.println("请重新选择");
            }
        }

    }
}
