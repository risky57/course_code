package com.lanou3g.demo;

public class Family {

    private Man man;
    private Woman woman;
    private Child child;

    public Family(Man man, Woman woman, Child child){
        this.man = man;
        this.woman = woman;
        this.child = child;

        man.setWife(woman);
        man.setChild(child);

        woman.setHusband(man);
        woman.setChild(child);

        child.setFather(man);
        child.setMother(woman);
    }

    public void travel(){
        System.out.printf("%s 和 %s 带着 %s 去旅行\n", man.getName(), woman.getName(), child.getName());
    }


}
