package com.lanou3g.demo;

public class Child extends Person {

    private Man father;
    private Woman mother;

    public void doHomework(){
        System.out.printf("%s 和 %s 帮助 %s 做作业\n", father.getName(), mother.getName(), getName());
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }
}

