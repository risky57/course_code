package com.lanou3g.demo;

public class Man extends Person {

    private Woman wife;
    private Child child;

    public void makeMoney(){
        System.out.printf("%s 挣钱给 %s 和 %s 花\n", getName(), wife.getName(), child.getName());
    }

    public void setWife(Woman wife) {
        this.wife = wife;
    }

    public void setChild(Child child) {
        this.child = child;
    }
}
