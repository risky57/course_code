package com.lanou3g.demo;

public class Main {
    public static void main(String[] args) {
        String str = "name=张三,height=1.8,age=15 " +
                "age=20,name=李四,height=1.6 " +
                "name=王五,age=22,height=1.76";
        String[] strings = str.split(" ");
        Person[] persons = new Person[strings.length];

        for (int i = 0; i < strings.length; i++) {
            Person person = new Person();
            String s = strings[i];
//            String[] split = s.split("[=,]");
//            p.setName(split[1]);
//            p.setAge(Integer.parseInt(split[3]));
//            p.setHeight(Float.parseFloat(split[5]));
            String[] p = s.split(",");// [name=张三, age=15, height=1.6]
            for (int j = 0; j < p.length; j++) {
                String[] f = p[j].split("=");// [name, 张三] [age, 15] [height, 1.6]
                switch (f[0]) {
                    case "name":
                        person.setName(f[1]);
                        break;
                    case "age":
                        person.setAge(Integer.parseInt(f[1]));
                        break;
                    case "height":
                        person.setHeight(Float.parseFloat(f[1]));
                        break;
                }
            }

            persons[i] = person;
        }

        for (int i = 0; i < persons.length; i++) {
            System.out.println(persons[i].getName() + persons[i].getAge() + persons[i].getHeight());
        }

        System.out.println("_------------------");

        Man smallHead = new Man();
        smallHead.setName("小头爸爸");
        smallHead.setAge(35);

        Woman mom = new Woman();
        mom.setName("围裙妈妈");
        mom.setAge(18);

        Child son = new Child();
        son.setName("大头儿子");
        son.setAge(1);

//        Woman w = new Woman();
//        w.setName("刘嘉玲");
//
//        Child c = new Child();
//        c.setName("小王");

//        smallHead.setWife(mom);
//        smallHead.setChild(son);
//
//        mom.setHusband(smallHead);
//        mom.setChild(son);
//
//        son.setFather(smallHead);
//        son.setMother(mom);
        Family family = new Family(smallHead, mom, son);

        smallHead.print();
        mom.print();
        son.print();

        smallHead.makeMoney();
        mom.cook();
        son.doHomework();

        family.travel();


    }
}
