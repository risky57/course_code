package com.lanou3g.demo;

public class Woman extends Person {
    private Man husband;
    private Child child;

    public void cook(){
        System.out.printf("%s 给 %s 和 %s 做饭\n", getName(), husband.getName(), child.getName());
    }

    public void setHusband(Man husband) {
        this.husband = husband;
    }

    public void setChild(Child child) {
        this.child = child;
    }
}
