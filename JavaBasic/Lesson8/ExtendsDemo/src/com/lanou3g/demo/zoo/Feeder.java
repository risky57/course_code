package com.lanou3g.demo.zoo;

public class Feeder {

    public static final int TYPE_P = 1;
    public static final int TYPE_M = 2;


//    public void feed(Penguin penguin){
//        penguin.eat();
//    }
//
//    public void feed(Mouse mouse){
//        mouse.eat();
//    }


    public void feed(Animal animal){
        animal.eat();
    }

    public Animal catchOne(int type){
        switch (type) {
            case 1:
                // 匿名对象
                return new Penguin("1企鹅", 1005);
            case 2:
                Mouse m = new Mouse("2老鼠", 1006);
                return m;
        }
        return null;
    }

}
