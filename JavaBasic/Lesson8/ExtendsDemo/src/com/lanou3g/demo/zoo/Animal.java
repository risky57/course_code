package com.lanou3g.demo.zoo;

import java.util.Objects;

public class Animal extends Object {

    // 在java中如果一个类没有明确指定父类类型,
    // 那么它就继承Object类
    // Java中类是单继承的, 也就是说一个类同时只能直接继承一个类

    public static final String DOG_TYPE_LBLD = "xxx拉布拉多";

    /*
    如果父类的属性是private, 那么子类不能直接调用该属性,
    但是可以通过父类的getter&setter方法间接的使用该属性.
     */
    private String name;
    private int id;

//    public Animal(){
//
//    }

    public Animal(String name, int id) {
        super();
        this.name = name;
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return id == animal.id &&
                Objects.equals(name, animal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }

    /**
     * 重写父类Object类的equals方法,
     * 重新定义两个对象是否相等的规则
     *
     * @param obj
     * @return
     */
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        // 判断obj对象是不是Animal类型的
//        if (obj instanceof Animal) {
//            Animal other = (Animal) obj;
//            return this.name.equals(other.name) && this.id == other.id;
//        }
//        return false;
//    }



    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }

    /**
     * 重写Object类的toString()方法.
     * 返回该对象的所有属性
     *
     * @return
     */

//    public String toString() {
//        StringBuilder sb = new StringBuilder();
//        sb.append("Animal {");
//        sb.append("name = ");
//        sb.append(name)
//                .append(", id = ")
//                .append(id)
//                .append("}");
//        return sb.toString();
//    }

    public void eat() {
        System.out.println(name + "正在吃");
    }

    public void sleep() {
        System.out.println(name + "正在睡觉");
    }

    public void introduction() {
        System.out.println("大家好, 我是" + id + "号, 我叫" + name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
