package com.lanou3g.demo.zoo;

public class Mouse extends Animal{

    public Mouse(String name, int id){
        super(name, id);
    }

    public void eat(){
        System.out.println(getName() + "正在吃大米");
    }
    public void stealOil(){
        System.out.println(getName() + "正在偷油");
    }
}
