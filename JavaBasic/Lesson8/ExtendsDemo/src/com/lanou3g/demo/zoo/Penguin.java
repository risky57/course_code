package com.lanou3g.demo.zoo;

public class Penguin extends Animal {

    /*
    原则:
    在创建一个对象的时候, 调用构造方式时,
    一定会先调用父类的构造方法
     */
    public Penguin(String name, int id){
//        super("企鹅", 3);
        super(name, id);
    }

    /*
    方法的重写:
    在子类中把可以继承过来的方法重新声明一遍
    访问权限不能比父类的更严格
     */
    @Override
    public void eat(){
        // 调用父类的方法
        super.eat();
        System.out.println(getName() + "在吃鱼");
    }

    public void catchFish() {
        // 使用从父类继承来的属性
        System.out.println(getName() + "正在捕鱼");
    }

}
