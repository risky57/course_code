package com.lanou3g.demo;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Random;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        // 随机数
        Random r = new Random();
        for (int i = 0; i < 100; i++) {
//            System.out.println(r.nextFloat());
        }
        // nextInt(int bound) 随机获取一个[0, bound)范围的整数

        // 日期类
        // 单例模式 / 静态工厂  23种设计模式之一
        Calendar calendar = Calendar.getInstance();

        // 获取日期相关的数据
        int i = calendar.get(Calendar.DAY_OF_WEEK);
        System.out.println(i);

        // 日期计算
        // 给日期中的某个属性加某个值
        calendar.add(Calendar.YEAR, 5);
        calendar.set(1988, 9, 25);
        int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
        System.out.println(dayOfYear);

        // 正则表达式
        // 作用:
        /*
        1. 用来判断某个字符串是不是符合某个规则
        2. 在某个字符串中查找某个规则的子串
        asefoolskdjfefolskdjfooolskdfooooolskdjflfo
         */
        String str = "asefool* skdjfef;*ols*kdjf ;ooolsk    dfoo   * oools$* kdjflfodd";
        // 正则表达式中的 +, 代表匹配前面的字符1个或多个
        String[] strings = str.split("(fo)+");
        // 正则表达式中的 *, 代表匹配前面的字符0个或多个
//        String[] strings = str.split("fo*");
        // | 代表或
//        String[] strings = str.split("\\*| |;|\\$");
        // [ ]代表匹配一个字符, 匹配规则为[]里面的任意一个
//        String[] strings = str.split("[* ;$]{2,}");
        System.out.println(Arrays.toString(strings));

        String email = "a1222223@b.c";
        String rex = "[\\w]{6,}@[a-zA-Z0-9]+\\.[a-zA-Z0-9]+";
        System.out.println(email.matches(rex));
        // \\s 匹配所有空白 包括空格/换行/制表符  |\n|\t|\r
        // \\w 英文/数字/_

        boolean b = Pattern.matches(rex, email);

        // 基本数据类型的包装类
        int a = 5;
        Integer n = 5;
        int num = Integer.parseInt("678");
        Byte x = 3;
        Short s = 5;
        Long l = 10L;
        Float f = 3.5F;
        float v = Float.parseFloat("7.8");
        Double d = 1.1;

        Character c = 'd';
        Boolean flag = false;
        Integer a1 = 100;
        Integer a2 = 100;
        Integer b1 = 200;
        Integer b2 = Integer.valueOf(200);
        System.out.println(a1 == a2);
        System.out.println(b1 == b2);

//        email.charAt(2000);
        String st = "name=张三,age=15,height=1.8 name=李四,age=20,height=1.6 name=王五,age=22,height=1.76";

//        Person[] persons = new Person[3];




    }
}
