package com.lanou3g.demo.pool;

import java.util.concurrent.LinkedBlockingQueue;

// 子线程
public class SubThread extends Thread {
    // 阻塞队列
    private static final LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();

    private boolean flag = true;

    @Override
    public void run() {
        try {
            while (flag) {
                Runnable r = queue.take();
                r.run();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void close(){
        flag = false;
    }

    public static void add(Runnable r){
        queue.add(r);
    }
}
