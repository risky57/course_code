package com.lanou3g.demo.pool;

public class RunTaskImpl implements RunTask {

    private int index;

    public RunTaskImpl(int index) {
        this.index = index;
    }

    @Override
    public void dododo() {
        Thread t = Thread.currentThread();
        System.out.println(t.getName() + " 任务" + index);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
