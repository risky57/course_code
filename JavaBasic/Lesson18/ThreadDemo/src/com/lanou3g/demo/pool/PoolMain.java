package com.lanou3g.demo.pool;

import java.util.concurrent.LinkedBlockingQueue;

public class PoolMain {

    public static void main(String[] args) throws InterruptedException {

        // 线程池

        // 实现一个简单的线程池
//        SubThread t1 = new SubThread();
//        t1.start();
//        SubThread t2 = new SubThread();
//        t2.start();
//        SubThread t3 = new SubThread();
//        t3.start();
//
//        for (int i = 0; i < 50; i++) {
//            final int index = i;
//            SubThread.add(new RunTaskImpl(index));
//        }

        MyThreadPool pool = new MyThreadPool(3);
        pool.start();

        for (int i = 0; i < 50; i++) {
            final int index = i;
            pool.execute(new Runnable() {
                @Override
                public void run() {
                    System.out.println("任务" + index);
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }


    }
}
