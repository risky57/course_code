package com.lanou3g.demo;

public class WaitDemo {
    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 5; i++) {
            new Waiter().start();
        }

        Thread.sleep(3000);
        System.out.println("主线程开始通知");
        synchronized (Waiter.o){
            Waiter.o.notify();
        }

        Thread.sleep(3000);
        System.out.println("主线程开始通知1");
        synchronized (Waiter.o){
            Waiter.o.notify();
        }

        Thread.sleep(3000);
        System.out.println("主线程开始通知2");
        synchronized (Waiter.o){
            Waiter.o.notify();
        }

        Thread.sleep(3000);
        System.out.println("主线程开始通知3");
        synchronized (Waiter.o){
            Waiter.o.notify();
        }

        Thread.sleep(3000);
        System.out.println("主线程开始通知4");
        synchronized (Waiter.o){
            Waiter.o.notify();
        }

        Thread.sleep(3000);
        System.out.println("主线程开始通知5");
        synchronized (Waiter.o){
            Waiter.o.notify();
        }


    }
}
