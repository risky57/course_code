package com.lanou3g.demo;

import java.util.*;

public class SyncDemo {
    private static List<Integer> data = new ArrayList<>();

    private static Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {

        MyRunnable r = new MyRunnable();

        // 1. 把线程不安全的集合转换为线程安全的
//        data = Collections.synchronizedList(data);
        // 2. 直接使用线程安全的集合对象
        data = new Vector<>();// 线程安全的集合
//        Hashtable 线程安全的Map    也叫做 字典
        // 3. 同步代码块

        /*
        开启5条线程,
        每条线程都向集合中放入100个数,
        总计500个
         */
        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                for (int j = 0; j < 100; j++) {

                    // 保证同时只能有一个线程访问该代码
                    synchronized (r){
                        data.add(j);
                    }

                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        // 为了保障5条线程都能执行完
        Thread.sleep(1000);
        System.out.println(data.size());
        /*
        1. 程序正常执行, 数据结果为500
        2. 程序正常执行, 输出结果小于500
        3. 抛出下标越界异常
         */



        Waiter w = new Waiter();
        // 同步的成员方法的锁是当前类的对象
        // 同步的静态方法的锁是当前的类

        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                for (int j = 0; j < 10; j++) {
                    w.doSth();
                }
            }).start();
        }

        Thread.sleep(1000);
        System.out.println(w.getIndex());


    }
}
