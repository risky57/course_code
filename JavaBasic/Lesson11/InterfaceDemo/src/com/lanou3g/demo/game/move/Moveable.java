package com.lanou3g.demo.game.move;

public interface Moveable {

    void move();
}
