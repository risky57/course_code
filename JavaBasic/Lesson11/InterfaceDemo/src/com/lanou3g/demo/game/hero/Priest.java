package com.lanou3g.demo.game.hero;

import com.lanou3g.demo.game.attack.MagicAttack;
import com.lanou3g.demo.game.move.HorseMove;

public class Priest extends Hero {

    public Priest() {
        attackable = new MagicAttack();
        moveable = new HorseMove();
    }
}
