package com.lanou3g.demo.game.hero;

import com.lanou3g.demo.game.attack.Attackable;
import com.lanou3g.demo.game.move.Moveable;

public class Hero {

    /*
    要求:
    战士: 用刀砍  走路
    法师: 用魔法攻击  闪现走路
    骑士: 用刀砍  骑马走路
    牧师: 用魔法攻击  骑马走路
     */
    protected Attackable attackable;
    protected Moveable moveable;

    public void attack(){
        attackable.attack();
    }

    public void move(){
        moveable.move();
    }

    public void setAttackable(Attackable attackable) {
        this.attackable = attackable;
    }

    public void setMoveable(Moveable moveable) {
        this.moveable = moveable;
    }
}
