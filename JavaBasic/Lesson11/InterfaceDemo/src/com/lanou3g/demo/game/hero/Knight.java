package com.lanou3g.demo.game.hero;

import com.lanou3g.demo.game.move.HorseMove;
import com.lanou3g.demo.game.attack.KnifeAttack;

public class Knight extends Hero {

    public Knight() {
        attackable = new KnifeAttack();
        moveable = new HorseMove();
    }
}
