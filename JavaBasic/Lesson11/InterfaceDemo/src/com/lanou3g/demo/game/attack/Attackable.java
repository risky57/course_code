package com.lanou3g.demo.game.attack;

public interface Attackable {

    void attack();

}
