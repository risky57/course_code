package com.lanou3g.demo.game.hero;

import com.lanou3g.demo.game.attack.KnifeAttack;
import com.lanou3g.demo.game.move.HorseMove;
import com.lanou3g.demo.game.move.NormalMove;

public class Warrior extends Hero {

    public Warrior() {
        // 为攻击和移动的两个接口赋值
        attackable = new KnifeAttack();
        moveable = new NormalMove();
    }
}
