package com.lanou3g.demo;

public class DivComputer implements Computable {
    @Override
    public int compute(int x, int y) {
        return x / y;
    }
}
