package com.lanou3g.demo;

import com.lanou3g.demo.game.attack.MagicAttack;
import com.lanou3g.demo.game.hero.Knight;
import com.lanou3g.demo.game.hero.Warrior;
import com.lanou3g.demo.game.move.HorseMove;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

//        Computable c = new AddComputer();
//        System.out.println(c.compute(10, 5));
//        c = new MinusComputer();
//        System.out.println(c.compute(10, 5));
//        c = new MulComputer();
//        System.out.println(c.compute(10, 5));
//        c = new DivComputer();
//        System.out.println(c.compute(10, 5));
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入第一个整数:");
        int x = sc.nextInt();
        System.out.println("请输入第二个整数:");
        int y = sc.nextInt();
        System.out.println("输入运算符:");
        String s = sc.next();

        int result = 0;
        Computable c = null;
        switch (s) {
            case "+":
//                result = x + y;
                c = new AddComputer();
                break;
            case "-":
//                result = x - y;
                c = new MinusComputer();
                break;
            case "*":
//                result = x * y;
                c = new MulComputer();
                break;
            case "/":
//                result = x / y;
                c = new DivComputer();
                break;
        }

//        Map<String, Computable> cm = new HashMap<>();
//        cm.put("+", new AddComputer());
//        cm.put("-", new MinusComputer());
//        cm.put("*", new MulComputer());
//        cm.put("/", new DivComputer());

//        Computable com = cm.get(s);

        if (c != null){
            result = c.compute(x, y);
        }
        System.out.println(result);

        System.out.println("-----------");

        Warrior warrior = new Warrior();
        warrior.attack();
        warrior.move();
        warrior.setAttackable(new MagicAttack());
        warrior.attack();
        warrior.setMoveable(new HorseMove());
        warrior.move();




    }
}
