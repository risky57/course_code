package com.lanou3g.demo;

import com.lanou3g.demo.shop.*;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        Goods g1 = new Goods("商品1", 10);
        Goods g2 = new Goods("商品2", 20);
        Goods g3 = new Goods("商品3", 30);
        Goods g4 = new Goods("商品4", 40);

        ShoppingCart cart = new ShoppingCart();
        cart.add(g1);
        cart.add(g2);
        cart.add(g3);
        cart.add(g4);

        CashierDesk desk = new CashierDesk(cart);
        desk.setDiscountable(new DefaultDiscount());
        System.out.println(desk.checkout());

        List coll = new ArrayList();

        coll.add(1);
        coll.add(g1);

        System.out.println(coll.size());
//        Object o = coll.get(0);
//        Object o1 = coll.get(1);

        // 用的最多的情况, 集合中放的都是同一种数据类型的对象
        // 使用泛型
        // 规定集合中能存放的数据类型
        List<Goods> list = new ArrayList<>();
//        list.add(1); 只能放Goods对象
        list.add(g1);
        list.add(g2);
        list.add(g2);
        list.add(g2);
        list.add(g3);
        list.add(g4);
        // 根据下标获取数据
        Goods goods = list.get(1);
        list.remove(1); // 根据下标移除集合中的某个元素
        list.remove(g2);// 把某个元素从集合中移除
        System.out.println(list.size());// 获取集合中元素的数量

        /*
        ArrayList 内部使用数组实现的, 查找效率非常高, 因为内存是连续的.
        当删除或插入某个元素的时候, 其他大部分元素都会发生移动, 所以效率比较低.
        LinkedList 内部使用链表实现, 查找某个元素的时候只能从开始位置遍历,
        所以效率较低.
        但是插入和删除某个元素的时候其他元素不用移动位置, 效率较高

         */
        // 判断集合中有没有g1这个对象
        boolean b = list.contains(g1);
        // 清空集合
//        list.clear();
        // 判断某个元素在集合中的下标
        int index = list.indexOf(g1);// -1
        // 判断集合是不是空的
        boolean empty = list.isEmpty();
        Goods[] array = list.toArray(new Goods[list.size()]);

        List<Goods> subList = list.subList(0, 3);
        subList.remove(1);
        System.out.println("原集合长度:" + list.size());
        // 集合的遍历
        // 1.
        for (int i = 0; i < list.size(); i++) {
            Goods g = list.get(i);
            // 移除商品价格为10的元素
            if (g.getPrice() == 10F) {
                list.remove(g);
                i--;
            }
        }
        // 2. 增强for循环
        // 在循环中不能更改集合的长度
        for (Goods g : list) {
//            if (g.getPrice() == 10F){
//                list.remove(g);
//            }
        }
        // 3. 迭代器
        Iterator<Goods> iterator = list.iterator();
        while (iterator.hasNext()) {
            Goods g = iterator.next();
        }
        // 4. java8 之后的
        float f = 0;
        list.forEach(new Consumer<Goods>() {
            @Override
            public void accept(Goods goods) {
                System.out.println(f);
            }
        });

        // 4. java8 lambda 表达式
        list.forEach(g -> {

        });
        list.clear();
        list.add(new Goods(3, "e商品1", 22));
        list.add(new Goods(5, "d商品2", 11));
        list.add(new Goods(8, "a商品3", 33));
        list.add(new Goods(1, "i商品4", 20));
        list.add(new Goods(2, "c商品5", 18));
        list.add(new Goods(6, "f商品6", 44));
        list.add(new Goods(7, "h商品7", 25));
        list.add(new Goods(9, "g商品8", 80));

//        list.get(1000);
        // 按价格由低到高的顺序打印出集合中商品价格在30 ~ 50之间的商品名称
        // 对集合排序
        // 前提: 集合中的元素一定是可比较大小的
        // 实现了Comparable接口的类可以自定义比较规则
        Collections.sort(list, new Comparator<Goods>() {
            @Override
            public int compare(Goods o1, Goods o2) {
                if (o1.getId() != o2.getId()){
                    return o1.getId() - o2.getId();
                }
                return o1.getPrice() - o2.getPrice() > 0 ? 1 : -1;

//                return o1.getName().compareTo(o2.getName());
            }
        });
        for (int i = 0; i < list.size(); i++) {
            Goods g = list.get(i);
            if (g.getPrice() >= 20 && g.getPrice() <= 50) {
                System.out.println(g.getName());
            }
        }
        //
//        list.sort(new Comparator<Goods>() {
//            @Override
//            public int compare(Goods o1, Goods o2) {
//                return 0;
//            }
//        });

        // java8 中的集合处理
        System.out.println("----------Java8 处理");
        // 找出符合条件的数据并根据id排序, 获取商品名称的集合
        // 函数式编程
        List<String> names = list.stream()
                .filter(goods1 -> goods1.getPrice() >= 20 && goods1.getPrice() <= 50)
                .sorted(Comparator.comparingInt(Goods::getId))
                .map(Goods::getName)
//                .forEach(g -> System.out.println(g));
                .collect(Collectors.toList());
        System.out.println(names);


        Person p = new Person();
        p.next = new Person();
        p.next.next = new Person();
        p.next.next.next = new Person();

        List<Integer> ints = new ArrayList<>();
        Integer i1 = new Integer(2);
        ints.add(i1);
        ints.add(4);
        ints.add(6);
        ints.add(8);
        ints.remove(i1);
        System.out.println(ints);



    }

    private static class TestConsumer implements Consumer<Goods>{

        @Override
        public void accept(Goods goods) {

        }
    }

}
