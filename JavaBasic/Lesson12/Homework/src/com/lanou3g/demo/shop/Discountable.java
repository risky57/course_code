package com.lanou3g.demo.shop;

public interface Discountable {

    float discount(float totalMoney);

}
