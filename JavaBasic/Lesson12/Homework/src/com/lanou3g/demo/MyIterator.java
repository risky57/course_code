package com.lanou3g.demo;

import com.lanou3g.demo.shop.Goods;

public interface MyIterator {

    /**
     * 用来判断是否有下一个
     * @return
     */
    boolean hasNext();

    /**
     * 获取下一个元素
     * @return
     */
    Goods next();

}
