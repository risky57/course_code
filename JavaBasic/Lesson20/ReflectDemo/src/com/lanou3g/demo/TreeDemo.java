package com.lanou3g.demo;

public class TreeDemo {
    public static void main(String[] args) {

        Node root = new Node(1);

        root.setLeft(new Node(2));
        root.setRight(new Node(3));

        root.getLeft().setLeft(new Node(4));
        root.getLeft().setRight(new Node(5));

        root.getRight().setLeft(new Node(6));
        root.getRight().setRight(new Node(7));

        char[] ch = {'a', 'b', 'c'};
        System.out.println(ch);

        int[] in = {1, 2, 3, 4};
        System.out.println(in);


        print(root);

    }

    private static void print(Node node){
        if (node != null) {
            if (node.getLeft() != null){
                System.out.println(node);
                print(node.getLeft());
            }
            if (node.getRight() != null){
                print(node.getRight());
            }
        }

    }

}
