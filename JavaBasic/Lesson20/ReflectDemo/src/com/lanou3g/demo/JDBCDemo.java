package com.lanou3g.demo;

import java.sql.*;

public class JDBCDemo {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        String url = "jdbc:mysql://127.0.0.1:3306/new_db";
        String user = "root";
        String password = "root";
        Connection conn = DriverManager.getConnection(url, user, password);
        Class.forName("com.mysql.jdbc.Driver");

        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("select age from t_users where length(name) = 2 and no > 50 order by age ");
        //4.处理数据库的返回结果(使用ResultSet类)
        while (rs.next()) {
            System.out.println(rs.getString("username") + " "
                    + rs.getString("passwd"));
        }


        //关闭资源
        rs.close();
        st.close();
        conn.close();
    }
}
