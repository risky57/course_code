package com.lanou3g.demo;

public class Node {

    private int i;

    private Node left;
    private Node right;

    public Node(int i) {
        this.i = i;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    @Override
    public String toString() {
        return String.valueOf(i);
    }
}
