package com.wuqi.dev;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JDBCDemo {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {

        Class.forName("com.mysql.jdbc.Driver");

        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/new_db", "root", "root");
        System.out.println(conn.getClass().getName());
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from t_students;");
        List<Student> students = new ArrayList<>();
        while (resultSet.next()){
            int stuId = resultSet.getInt(1);
            String stuName = resultSet.getString(2);
            Date birthday = resultSet.getDate(3);
            String stuGender = resultSet.getString(4);
            Student s = new Student(stuId, stuName, birthday, stuGender);
            students.add(s);
        }
        students.forEach(System.out::println);

        resultSet.beforeFirst();

        System.out.println("------------");

        PreparedStatement ps = conn.prepareStatement("");


        resultSet.close();
        statement.close();
        conn.close();


    }
}
