package com.wuqi.dev;

import java.sql.Date;
import java.sql.Timestamp;

public class Student {
    private int stuId;
    private String stuName;
    private Date birthday;
    private String gender;

    public Student() {
    }

    public Student(int stuId, String stuName, Date birthday, String gender) {
        this.stuId = stuId;
        this.stuName = stuName;
        this.birthday = birthday;
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Student{" +
                "stuId=" + stuId +
                ", stuName='" + stuName + '\'' +
                ", birthday=" + birthday +
                ", gender='" + gender + '\'' +
                '}';
    }

    public int getStuId() {
        return stuId;
    }

    public void setStuId(int stuId) {
        this.stuId = stuId;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
