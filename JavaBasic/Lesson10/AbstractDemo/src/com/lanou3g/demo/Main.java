package com.lanou3g.demo;

import com.lanou3g.demo.hero.Hero;
import com.lanou3g.demo.hero.Mage;
import com.lanou3g.demo.hero.Rogue;
import com.lanou3g.demo.hero.Warrior;
import com.lanou3g.demo.player.MaterPlayer;
import com.lanou3g.demo.player.Player;
import com.lanou3g.demo.robot.Robot;
import com.lanou3g.demo.robot.WoodRobot;

public class Main {
    public static void main(String[] args) {
        Hero h1 = new Mage("吉安娜");
        Hero h2 = new Rogue("伊利丹");
        Hero h3 = new Warrior("基尔加丹");

        // 匿名内部类对象
        // 代表创建了一个没名的类继承了Hero类,
        // 并且重写了两个方法
        Hero h4 = new Hero("") {
            @Override
            public void attack() {

            }

            @Override
            public void move() {

            }
        };
        h4.attack();

        Player p1 = new MaterPlayer("三季稻");
//        Player p2 = new Player("高帅");
        // 具体的业务逻辑
        p1.select(h1);
        p1.controlToMove();
        p1.controlToAttack();
        p1.select(h2);
        p1.controlToMove();
        p1.controlToAttack();
        p1.select(new Mage("法师"));
        p1.select(new Mage("法师2"));
//        p1.heroAttack();

//        p2.select(h3);
//        p2.controlToMove();
//        p2.controlToAttack();

        Robot r = new WoodRobot();
        r.create();

        Cook cook = new PCook();
        cook.cooking();
        /*
        1. 单例模式
        2. 静态工厂 工厂模式 工厂方法
        3. 模板方法
        4. 建造者模式
         */

    }
}
