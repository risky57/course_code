package com.lanou3g.demo.player;

import com.lanou3g.demo.hero.Hero;

public abstract class Player{

    private String name;
    private Hero hero;

    public Player(String name){
        this.name = name;
    }

    public final void controlToAttack(){
        System.out.print(name + "控制");
        this.heroAttack();
    }

    protected abstract void heroAttack();

    public final void controlToMove(){
        System.out.print(name + "控制");
        hero.move();
    }

    public void select(Hero hero){
        this.hero = hero;
    }

    public String getName() {
        return name;
    }

    public Hero getHero() {
        return hero;
    }
}
