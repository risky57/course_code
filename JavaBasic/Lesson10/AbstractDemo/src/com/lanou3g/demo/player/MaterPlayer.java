package com.lanou3g.demo.player;

public class MaterPlayer extends Player {
    public MaterPlayer(String name) {
        super(name);
    }

    @Override
    protected void heroAttack() {
        getHero().attack();
        getHero().attack();
        getHero().attack();
    }
}
