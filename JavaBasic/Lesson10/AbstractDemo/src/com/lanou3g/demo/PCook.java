package com.lanou3g.demo;

public class PCook extends Cook {

    @Override
    protected void putVegetable() {
        System.out.println(" 放土豆丝");
    }

    @Override
    protected void putFlavour() {
        System.out.println("放盐");
    }
}
