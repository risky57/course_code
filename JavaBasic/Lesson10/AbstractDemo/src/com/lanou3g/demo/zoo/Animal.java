package com.lanou3g.demo.zoo;

public abstract class Animal {

    public abstract void eat();

    public abstract void move();

}
