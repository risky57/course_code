package com.lanou3g.demo.zoo;

public abstract class GrassAnimal extends Animal {

    @Override
    public void eat() {
        System.out.println("吃草");
    }
}
