package com.lanou3g.demo.hero;

public class Rogue extends Hero {
    public Rogue(String name) {
        super(name);
    }

    @Override
    public void attack() {
        System.out.printf("盗贼: %s 使用匕首攻击\n", getName());
    }

    @Override
    public void move() {
        System.out.printf("盗贼: %s 使用暗影步移动\n", getName());
    }
}
