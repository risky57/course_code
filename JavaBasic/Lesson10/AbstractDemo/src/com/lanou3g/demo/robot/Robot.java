package com.lanou3g.demo.robot;

public abstract class Robot {

    /**
     * 机器人组装方法. 模板方法模式
     */
    public final void create(){
        System.out.println("组装脑袋");
        createHead();
        System.out.println("组装身体");
        createBody();
        System.out.println("组装胳膊");
        createArm();
        System.out.println("组装大腿");
        createLeg();
    }

    protected abstract void createHead();
    protected abstract void createBody();
    protected abstract void createArm();
    protected abstract void createLeg();

}
