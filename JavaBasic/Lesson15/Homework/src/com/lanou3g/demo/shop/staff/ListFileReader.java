package com.lanou3g.demo.shop.staff;

import com.lanou3g.demo.shop.entity.GoodsList;
import com.lanou3g.demo.shop.equipment.GoodsShelf;

public interface ListFileReader {

    boolean loadListToShelf(GoodsList list, GoodsShelf shelf);

}
