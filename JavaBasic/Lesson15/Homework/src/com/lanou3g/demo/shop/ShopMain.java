package com.lanou3g.demo.shop;

import com.lanou3g.demo.shop.entity.GoodsList;
import com.lanou3g.demo.shop.equipment.GoodsBasket;
import com.lanou3g.demo.shop.equipment.GoodsCart;
import com.lanou3g.demo.shop.equipment.GoodsShelf;
import com.lanou3g.demo.shop.equipment.HandEmpty;
import com.lanou3g.demo.shop.exception.GoodsCartFullException;
import com.lanou3g.demo.shop.staff.Cashier;
import com.lanou3g.demo.shop.staff.JsonListReader;
import com.lanou3g.demo.shop.staff.Worker;
import com.lanou3g.demo.shop.staff.XmlListReader;

public class ShopMain {

    public static void main(String[] args) {

        GoodsShelf snackShelf = new GoodsShelf();
        GoodsList snackList = new GoodsList("零食", "snack-list.json");
        Worker worker = new Worker();
        worker.setListFileReader(new JsonListReader());
        worker.setName("高帅儿");
        Cashier cashier = new Cashier();

        GoodsCart hand = new HandEmpty();
        GoodsCart basket = new GoodsBasket();

        Consumer consumer = new Consumer();

        boolean b = worker.loadListToShelf(snackList, snackShelf);
        if (b) {
            consumer.selectGoodsCart(basket);
            consumer.selectShelf(snackShelf);
            try{
                consumer.buy(10002, 5);
                consumer.buy(10008, 8);
            }catch (GoodsCartFullException e){
                String message = e.getMessage();
                System.out.println(message);
            }
            consumer.checkout(cashier);


            snackShelf.print();
        }


    }
}
