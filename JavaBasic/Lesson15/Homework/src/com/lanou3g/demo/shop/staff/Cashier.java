package com.lanou3g.demo.shop.staff;

import com.lanou3g.demo.shop.base.Staff;
import com.lanou3g.demo.shop.equipment.GoodsCart;

public class Cashier extends Staff {

    public float check(GoodsCart cart){
        float money = cart.compute();
        return money;
    }
}
