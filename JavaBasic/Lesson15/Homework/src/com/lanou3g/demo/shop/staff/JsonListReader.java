package com.lanou3g.demo.shop.staff;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lanou3g.demo.shop.base.GoodsContainer;
import com.lanou3g.demo.shop.entity.GoodsList;
import com.lanou3g.demo.shop.equipment.GoodsShelf;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.List;

public class JsonListReader implements ListFileReader {
    @Override
    public boolean loadListToShelf(GoodsList list, GoodsShelf shelf) {

        try {
            Reader r = new FileReader(list.getPath());
            Type t = new TypeToken<List<GoodsContainer.GoodsCell>>(){}
                .getType();
            List<GoodsContainer.GoodsCell> data =
                    new Gson().fromJson(r, t);
            shelf.getCells().addAll(data);
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        return false;
    }
}
