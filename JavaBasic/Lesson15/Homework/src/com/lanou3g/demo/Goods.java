package com.lanou3g.demo;

public class Goods {

    /**
     * goods : {"id":10001,"name":"甜辣鸭脖","price":12.9,"unit":"袋"}
     * quantity : 10
     */

    private GoodsBean goods;
    private int quantity;

    public GoodsBean getGoods() {
        return goods;
    }

    public void setGoods(GoodsBean goods) {
        this.goods = goods;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public static class GoodsBean {
        /**
         * id : 10001
         * name : 甜辣鸭脖
         * price : 12.9
         * unit : 袋
         */

        private int id;
        private String name;
        private double price;
        private String unit;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }
    }
}
