package com.lanou3g.demo;

import java.io.Serializable;

public class Goods implements Serializable {
    private String name;
    // 该属性不会写入到数据流中
    private transient int price;

    @Override
    public String toString() {
        return "Goods{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}' + '\n';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
