package com.lanou3g.demo;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileReaderDemo {

    public static void main(String[] args) {
        // 文件字符输入流
        // 用来读取文本文件中的内容
        FileReader reader = null;
        try {
            reader = new FileReader("Text.txt");
            // 一次读一个字符
//            int c = reader.read();
//            while (c != -1){
//                System.out.println((char)c);
//                c = reader.read();
//            }
            // 一次读多个字符
            char[] buf = new char[8];
            // 将输入流中的字符读取到字符数组中
            // 返回值代表读出字符的数量
//            int length = reader.read(buf);
//            while (length != -1){
//                String s = new String(buf, 0, length);
//                System.out.println(s);
//                length = reader.read(buf);
//            }
            int length = -1;
            while ((length = reader.read(buf)) != -1 ){
                String s = new String(buf, 0, length);
                System.out.println(s);
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }

}
