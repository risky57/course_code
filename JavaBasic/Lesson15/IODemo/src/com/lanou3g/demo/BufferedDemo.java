package com.lanou3g.demo;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BufferedDemo {
    public static void main(String[] args) throws IOException {

        // 带缓冲的流
        // 数据流的转换
//        InputStream is = new FileInputStream("Goods.txt");
        // 把一个字节流转换为字符流
//        InputStreamReader reader = new InputStreamReader(is);
        // 字符流可以转换为处理效率更高的BufferedReader
        // BufferedReader: 带缓冲的字符流
        // 特点: 读文本文件的时候, 可以一次读一行
        FileReader r = new FileReader("Goods.txt");
        BufferedReader br = new BufferedReader(r);
        List<Goods> goodsList = new ArrayList<>();
        String line = null;
        while ((line = br.readLine()) != null){
            String[] strings = line.split(",");
            String name = strings[0];
            int price = Integer.parseInt(strings[1]);
            Goods g = new Goods();
            g.setName(name);
            g.setPrice(price);
            goodsList.add(g);
        }

        // 后开的流先关
        br.close();
        r.close();

        System.out.println(goodsList);

        System.out.println("----------");
        // 装饰者模式
        FileWriter fw = new FileWriter("Test.txt");
        BufferedWriter writer = new BufferedWriter(fw);

        writer.write("abcd");
        writer.write("sdfsd");
        writer.write("sfeg");
        writer.write("egwerh");
        // 手动冲一下
        writer.flush();
        writer.write("ljpwep");
        writer.write("ljpwep");
        writer.write("ljpwep");
        writer.write("ljpwep");
//        writer.flush();
        writer.close();

    }
}
