package com.lanou3g.demo;

import java.io.*;

public class CopyDemo {
    public static void main(String[] args) {
        // 使用字节流实现文件的复制
        // 原文件的路径
        String srcPath = "/Users/Risky/Downloads/movie.mp4";
        // 目的文件
        String desPath = "/Users/Risky/Desktop/copy.mp4";
        FileInputStream is = null;
        ByteArrayOutputStream os = null;
        try {
            is = new FileInputStream(srcPath);
            os = new ByteArrayOutputStream();
            byte[] buff = new byte[1024 * 1024];
            int length = -1;
            // 把输入流中的数据放入数组,
            // 并判断是否放完
            while ((length = is.read(buff)) != -1){
                os.write(buff, 0, length);
            }
            byte[] bytes = os.toByteArray();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
