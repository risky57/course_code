package com.lanou3g.demo;

import java.io.FileInputStream;
import java.io.IOException;

public class InputStreamDemo {
    public static void main(String[] args) {
        // 字节输入流
        // 这种写法, 会自动对数据流进行close
        // Java8 以后才能用
        try (
                FileInputStream is = new FileInputStream("Music.txt")
        ) {
            byte[] buf = new byte[4];
            int length = -1;
            while ((length = is.read(buf)) != -1){
                String s = new String(buf, 0, length);
                System.out.println(s);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
