package com.lanou3g.demo;

import java.io.*;

public class ObjectDemo {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        FileInputStream fis = new FileInputStream("object.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object o = ois.readObject();
        Goods g = (Goods) o;
        System.out.println(g.getName());
        System.out.println(g.getPrice());

        ois.close();
        fis.close();

        //

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write("dalalalalal".getBytes());
        byte[] bytes = baos.toByteArray();
    }
}
