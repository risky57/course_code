package com.lanou3g.demo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Main {

    public static void main(String[] args) {
        // IO流
        // 字符输出流的其中一个子类:
        // 文件输出流, 可以进行文件的写操作
        File file = new File("/Users/Risky/Desktop/");
        FileWriter writer = null;
        try {
            writer = new FileWriter("Text.txt");
            //
//            writer.write("锄禾日当午\n");
//            writer.write("清明上河图\n");
            list(file, writer);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭数据流
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static void list(File file, FileWriter writer) throws IOException {

        if (file.isFile()) {
            String str = "文件名: " + file.getName() + '\n';
            writer.write(str);
        } else if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                list(f, writer);
            }
        }

    }

}
