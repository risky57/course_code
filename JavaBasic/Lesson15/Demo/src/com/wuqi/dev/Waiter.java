package com.wuqi.dev;

public class Waiter extends Thread {

    private String name;

    public Waiter(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        doSth();
    }

    private void doSth() {
        System.out.println(name + "开始工作");
        ThreadDemo.sleep(200);
        System.out.println(name + "做了部分工作, 开始等待通知");
        synchronized (ThreadDemo.lock) {
            try {
                ThreadDemo.lock.wait(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(name + "等待完成, 继续干活");
        System.out.println(name + "干完活了");
    }
}
