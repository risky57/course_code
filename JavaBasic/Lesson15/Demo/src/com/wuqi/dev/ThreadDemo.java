package com.wuqi.dev;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

public class ThreadDemo {
    private static long money = 250;
    public static final Object lock = new Object();

    private static List<Integer> list = new Vector<>();

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            final int index = i;
            new Thread(() -> {
                for (int j = 0; j < 10; j++) {
                    list.add(index + j);
                    sleep(1);
                }
            }).start();
        }

        sleep(1000);
        System.out.println(list.size());

    }

    public static void sleep(int t) {
        try {
            Thread.sleep(t);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
