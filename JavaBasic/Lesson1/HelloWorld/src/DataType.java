public class DataType {
    public static void main(String[] args) {
        // 数据类型
        /*
        Java中数据类型分为两大类:
        1. 基本数据类型
        2. 引用数据类型
        在前期的Java基础中, 基本数据类型都会学到,
        引用数据类型只会学习一种: String 字符串
         */
        // 基本数据类型:
        // 整型: byte short int long
        //      8位1字节 2字节 4字节 8字节
        //     -128~127
        // 浮点型: double float 小数
        // 布尔: boolean 值: true  false
        // 字符: char 用来存储单个字符

        // 定义变量
        // 公式:
        // 数据类型 变量名 = 初始值;

        float f = 2.8F;
        int age = 10;
        long l = 100;
        boolean b = true;
        // 数据类型的转换
        // 原则: 范围小的数据类型可以自动转换为范围大的数据类型
        // 如果需要把大范围的数赋值给小范围, 需要强制类型转换
        // 语法: 数据类型 变量名 = (需要转换的类型)值;

        byte b1 = (byte)300;
        System.out.println(b1);

        System.out.println(age << 2);

        char a = 'A';
        System.out.println(a);
        int i = a;
        System.out.println(i);
        // 字符与int之间根据ASCII码表进行转换

        char c = (char)58;
        System.out.println(c);

        // 先声明, 再赋值
        int height;
        float weight;

        height = 180;
        weight = 200F;
        // 变量规则:
        // 1. 先声明, 再使用
        // 2. 同一个名字的变量, 不能声明两次

        // 同时声明多个变量
        int x, y, z = 5;
        // 如果变量没有被赋值, 不能使用
        System.out.println(z);
        x = 10;
        y = 20;
        int temp = x;
        x = y;
        y = temp;
        // 标识符命名规范:
        // 类名 大驼峰 HelloWorld
        // 变量名 小驼峰 maxValueOfThree

        String name = "天下太平";
        System.out.println(name);

    }
}
