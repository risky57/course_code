public class MainTest {

    public static void main(String[] args) {
        System.out.println("Hello World");
        // 写代码的时候注意:
        // 如果出现编译错误, 就不要往下写了
        // 修改完再往下写

        /*
        快捷键:
        移动代码: command + shift + ↑/↓
        option + shift + ↑/↓
        在代码中间换行: shift + 回车
        复制当前行: command + c 剪切同理
        复制并粘贴当前行: command + d
        删除当前行: command + 退格键/y
        代码格式化: command + option + L
        代码提示: option + / (不是默认的)
        删除文件: command + 退格键
        修改文件名: shift + F6
        注释代码: command + /
         */
//        System.out.println();
        // 输出之后会拼接一个换行
//        System.out.print();
        // 输出之后不会换行


    }


}
