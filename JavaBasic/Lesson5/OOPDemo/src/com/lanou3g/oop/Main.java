package com.lanou3g.oop;
// 声明该类所在的包
// 规则: 该行代码必须写在第一行

import com.lanou3g.oop.shopping.Consumer;
import com.lanou3g.oop.shopping.Goods;

public class Main {
    // 以后再创建工程的时候, 必须指定包名
    // 并且所有的类必须都创建在该包或该包的子包下
    // 不允许直接在src文件夹下创建类

    public static void main(String[] args) {
        // 创建顾客类Consumer的对象
        // 创建对象的格式:
        // 数据类型 变量名 = 初始值;
        // 新建个类就相当于新建了一个新的引用数据类型

        // 新建Consumer类的对象, 对象名为 c1
        // 创建对象的公式: 类名 变量名 = new 类名();

        // 一般情况下, 某个类可以创建多个对象
        Consumer c1 = new Consumer();
        // 通过  对象名.属性名  的方式调用某个对象的属性

        // 某个类中的所有对象, 互相都是独立的.
        // 改变其中一个对象的状态时, 不会对其他对象产生影响.
        c1.name = "梁朝伟";
        c1.age = 18;
        c1.weight = 135.8F;
        Consumer c2 = new Consumer();
        Consumer c3 = new Consumer();
        Consumer c4 = new Consumer();

        // 当一个对象创建好之后, 会把该对象的所有属性
        // 都划分好内存.
        // 换句话说对象中的属性都是有初始值的
        // 整数型默认值: 0
        // 浮点型默认值: 0.0
        // boolean型默认值: false
        // char默认值: 数字0在ASCII码中对应的字符
        // 引用数据类型默认值: null
        System.out.println(c2.name);

        // 注意引用数据类型的特点
        Consumer c5 = c1;
        c1.name = "刘德华";
        System.out.println(c5.name);

        // 调用方法: 对象名.方法名(参数);
//        c1.buy();
//        c1.buy();
//        c1.buy();
//        c1.buy();
        // 遇到的第三个异常
        // 空指针异常
        // 调用了某个值为null的对象的方法或属性时,
        // 会抛出空指针异常
        Consumer c6 = null;
//        c6.name = "sss"; // 会抛出空指针异常
//        c6.buy(); // 会抛出空指针异常

        // 一个创建对象的过程
        // 1. 声明变量名
        // 2. 堆内存中划分内存空间
        // 3. 在内存空间中给初始值(对对象的初始化)

//        Scanner sc = new Scanner(System.in);
//        sc.nextInt()

        //

        // 创建10个商品
        Goods pen = new Goods("钢笔", 15F);
        Goods pencil = new Goods("铅笔", 1.8F);
        Goods phone = new Goods( "手机", 1299F);
        Goods water = new Goods("水", 3F);
        Goods clothes = new Goods("衣服", 288F);
//        water.print();
//        clothes.print();

        // 要求, 每个顾客都能买最多3件商品
        // 调用buy方法的时候, 打印出所购买的商品的总价

        // 刘德华买了水/铅笔/衣服
//        c1.shoppingCart[0] = water;
//        c1.shoppingCart[1] = pencil;
//        c1.shoppingCart[2] = clothes;
        c1.buy();
        // 创建一个顾客刘嘉玲, 买了手机/衣服/钢笔.
        // 然后调用buy()方法的时候,
        // 打印出顾客的名字, 商品清单, 商品总价
        Consumer liu = new Consumer();
        liu.name = "刘嘉玲";
        liu.age = -18;
        liu.weight = 200F;
//        liu.shoppingCart[0] = phone;
//        liu.shoppingCart[1] = clothes;
//        liu.shoppingCart[2] = pen;
        liu.buy();

        c1.eat("龙虾", 3);
        c1.eat("啤酒", 5);
        c1.eat("螃蟹", 10);

        System.out.println("------------");
        liu.add(pen);
        liu.add(clothes);
        liu.add(phone);
        liu.add(clothes);
        liu.add(clothes);
        liu.add(phone);
        liu.add(clothes);
        liu.add(phone);
        liu.add(pencil);
        liu.add(phone);
        liu.add(pencil);
        liu.add(phone);
        liu.add(pencil);
        liu.add(phone);
        liu.add(pencil);
//        liu.index = 0;
//        liu.index = -1;
        liu.add(water);
//        liu.shoppingCart[19] = phone;
        liu.buy();
    }

}
