package com.lanou3g.oop.shopping;

public class Goods {
    private String name;
    private float price;

    public Goods(String name, float price) {
        this.name = name;
        this.price = price;
    }

    void print() {
        System.out.printf("商品名称: %s, 单价: %s\n", name, price);
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }
}
