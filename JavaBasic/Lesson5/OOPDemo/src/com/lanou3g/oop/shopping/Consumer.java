package com.lanou3g.oop.shopping;

/**
 * 顾客类
 * @author 武奇
 */
public class Consumer {
    // 在类中直接声明的变量, 就是该类的属性
    // 声明的属性用来干嘛的?
    // 实际上是规定了一种规范:
    // 规范了该类的所有对象, 都有这几个属性

    // 属性的别名: 实例变量  成员变量
    public String name;
    public int age;
    public float weight;
    private ShoppingCart shoppingCart = new ShoppingCart();

    /*
    访问权限修饰符:
    可以修饰属性和方法, 用来规定某个属性或方法的可访问范围.
    1. private 私有的 只能在当前类访问
    2. 什么也不写 默认的 包访问权限 只能在同一个包下访问
    3. protected 受保护的 子类和包访问权限
    4. public 公开的 在任何位置都可以访问
    开发原则: 在以后的开发过程中, 所有的属性或方法的访问权限
    越小越好
     */

    // 方法(动态特征), 在某些编程语言中也称之为 函数
    /*
    方法的格式
    访问权限修饰符 返回值类型 方法名(参数列表) {
        方法体;
    }
    返回值: 代表该方法执行后的结果. 如果没有返回值, 该位置填 void
    方法只有调用才会执行.
    同一个方法可以调用多次.
     */
    public void buy() {
        // 在成员方法中调用了成员变量的时候,
        // 哪个对象调用的该方法, 那么该成员变量就是哪个对象的属性
        System.out.println("顾客姓名: " + name);
        float totalPrice = shoppingCart.calculate();
        System.out.println("商品总价为: " + totalPrice);
    }

    // 方法的参数:
    // 1. 参数可以有多个, 多个参数之间使用 ',' 隔开
    // 2. 方法的参数实际上也是制定了一个规范:
    //    参数声明的是什么类型, 那么在方法的调用时就得填入什么类型
    // 3. 方法中的参数也称作 形参(形式参数)
    // 4. 多个参数声明的是什么顺序, 那么调用时就得按该顺序填入参数
    public void eat(String food, int count) {
        System.out.println(name + "吃了" + food);
    }

    /**
     * 用来向购物车中添加商品.
     * @param goods 需要添加的商品
     */
    public void add(Goods goods){
        shoppingCart.add(goods);
    }
}
