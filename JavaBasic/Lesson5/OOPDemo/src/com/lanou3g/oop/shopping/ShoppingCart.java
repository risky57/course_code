package com.lanou3g.oop.shopping;

public class ShoppingCart {

    private int index;

    // 创建一个商品的数组
    private Goods[] goods = new Goods[5];

    /**
     * 计算购物车中货物的总价
     * @return 货物的总价
     */
    float calculate() {
        float totalPrice = 0F;
        // 遍历购物车, 取出数组中的每个元素(Goods的对象)
        for (int i = 0; i < goods.length; i++) {
            // 将Goods对象的price属性累加起来
            // 为了防止出现不必要的空指针异常
            // 在调用某个对象的属性之前,
            // 对其做非null的判断
            if (goods[i] != null){
                totalPrice += goods[i].getPrice();
                goods[i].print();
//                System.out.println("商品名: " + goods[i].name + ", 单价: " + goods[i].price);
            }
        }
        return totalPrice;

    }

    void add(Goods g){
        /*
        每次调用该方法, 都把goods对象放到
        数组中的index位置, 放完之后 index++.
        当然, 在放入数组之前, 需要判断 index 是否
        超出了数组的范围. 如果超出, 打印购物车已满
         */
        if (index < goods.length){
            this.goods[index] = g;
            index++;
        } else {
            Goods[] newGoods = new Goods[goods.length << 1];
            for (int i = 0; i < goods.length; i++) {
                newGoods[i] = goods[i];
            }
            newGoods[index] = g;
            index++;
            goods = newGoods;
            System.out.println("扩容: " + goods.length);

        }
    }
}
