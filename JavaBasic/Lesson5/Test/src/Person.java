public class Person {

    // 成员变量:
    // 也叫实例变量, 也叫属性
    // 直接在类中定义的变量.
    public String name;

    public void eat() {
        System.out.println("吃东西");
    }

    public void eat(String food){
        System.out.println("吃" + food);
    }

    public void eat(String food, int times){

    }

    public void eat(int times, String food){}

    //
}
