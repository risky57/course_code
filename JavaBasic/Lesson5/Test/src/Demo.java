public class Demo {

    public static void main(String[] args) {

        // 数据类型 变量名 = 初始值;
        Person p = new Person();
        /*
        for循环的语法
        for(循环变量初始化1; 循环条件2; 循环变量自增3){
            循环体4;
        }
        1 -> 2 -> 4 -> 3 -> 2 -> 4 -> 3 ...
        continue: 在循环中如果遇到continue关键字,
            那么会立刻停止当前次的循环, 直接进行下一次循环
        break: 在循环中如果遇到break关键字, 会直接停止循环
        int total = 5;
        int i = 3;

        total += i;
        total = 5 + 3;

        153 = 1³ + 5³ + 3³
         */

        int n = 153;
        for (int i = 100; i < 1000; i++) {
            int a = i / 100;
            int b = i % 100 / 10;
            int c = i % 10;
            if (a*a*a + b*b*b + c*c*c == i){
                System.out.println(i);
            }
        }
        int a = n / 100;
        int b = n % 100 / 10;
        int c = n % 10;



    }



}
