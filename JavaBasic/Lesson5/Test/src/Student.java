public class Student extends Person {

    String name;
    int age;

    Student(String s, int i){
        name = s;
        age = i;
    }


    public void study(){
        super.eat();
        System.out.println(super.name);
        System.out.println(this);
//        System.out.println(super);
    }

    // 方法的重写
    // 在子类中重新定义父类的方法就叫重写
    // 要求: 方法名/返回值/参数列表必须一致
    // 子类重写的方法的访问权限不能比父类的更严格

}
