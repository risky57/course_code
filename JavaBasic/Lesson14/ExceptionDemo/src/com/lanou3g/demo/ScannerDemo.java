package com.lanou3g.demo;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ScannerDemo {
    public static void main(String[] args) {
        // 由键盘输入5个整数, 求和
        Scanner sc = new Scanner(System.in);
        int sum = 0;

        for (int i = 0; i < 5; i++) {
            System.out.println("请输入第" + (i + 1) + "个数");
            try {
                int n = sc.nextInt();
                sum += n;
            } catch (InputMismatchException e) {
                System.out.println("输入错误, 请重新输入数字:");
                i--;
                sc = new Scanner(System.in);
            }
        }

        System.out.println("和为: " + sum);


    }
}
