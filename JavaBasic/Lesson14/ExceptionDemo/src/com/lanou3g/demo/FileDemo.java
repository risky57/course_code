package com.lanou3g.demo;

import java.io.File;
import java.io.IOException;

public class FileDemo {
    public static void main(String[] args) {
        // 文件类
        /*
        路径
        绝对路径: 从硬盘根目录开始一直到某个文件的路径
        /Users/Risky/Desktop/response.png
        相对路径: 相对于某个文件夹定位文件的路径

         */

        String path = "/Users/Risky/Desktop/response.png";
        File file = new File(path);
        System.out.println(file.getName());
        System.out.println(file.getParent());
        System.out.println(file.getPath());

        // 判断文件是否存在
        System.out.println(file.exists());
        // 判断是否是文件
        System.out.println(file.isFile());
        // 判断是否是文件夹
        System.out.println(file.isDirectory());
        // 列出该目录下的所有文件
        File[] files = file.listFiles();
        try {
            // 新建文件
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 获取文件大小, 单位是字节 Byte
        System.out.println(file.length());
        // 创建最后一级的文件夹
        // /Users/Risky/Desktop/F/G/H/
        file = new File("/Users/Risky/Desktop/F/G/H/");
        boolean b = file.mkdir();
        // 创建多级文件夹
        file.mkdirs();


        // 列出桌面上的所有文件和文件夹

        File desktop = new File("/Users/Risky/Desktop/");
        // 获取该文件夹下的所有文件
        File[] listFiles = desktop.listFiles();
        for (File f : listFiles) {
            // 文件名称
            String fileName = f.getName();
            // 判断是不是文件
            boolean isFile = f.isFile();
            // 获取文件大小
            long length = f.length();
            System.out.printf("文件名: %s, 是否是文件: %s, 文件大小: %s KB\n", fileName, isFile, length / 1024F);
        }

        // 列出桌面包括子文件中的所有图片名称/大小
        System.out.println("--------");
        list(new File("/Users/Risky/Documents/"));

    }

    public static void list(File file){
        if (file.isFile()){
            // 打印file信息
//            System.out.println(file.getPath());
            String name = file.getName();
            if (name.endsWith(".txt")){
                System.out.println(name);
            }
        } else if (file.isDirectory()){
            File[] files = file.listFiles();
            for (File f : files) {
                list(f);
            }
        }
    }


}
