package com.lanou3g.demo;

public class NumberNotBeNegativeException
        extends RuntimeException {

    public NumberNotBeNegativeException(int x, int y) {
        super("传入的参数必须为正数, 您传入的 x = " + x + ", y = " + y + " 不合法");
    }
}
