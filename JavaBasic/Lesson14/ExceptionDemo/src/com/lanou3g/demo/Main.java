package com.lanou3g.demo;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
//        main(args);
        // 异常
        /*
        算数异常
        数组越界 字符串越界 下标越界
        数字格式化异常
        空指针异常
        类型转换异常
         */
        // 异常会导致程序的中断
        // 异常也是类
        NullPointerException n = new NullPointerException("去你妹的空指针");

        System.out.println(n.getMessage());
        System.out.println("--------");
        // 在控制台打印该异常的日志
//        n.printStackTrace();
        System.out.println("----end----");

//        throw n;
        /*
        Java中的所有出错分为两类:
        Throwable
        1. Error 错误  通过一般的代码修改无法的处理的, 不能捕获
        2. Exception 异常 可以进行处理的

        Exception下面分为两类:
        1. RuntimeException 运行时异常
        2. 其他异常 强制检查型异常
         */

//        Object o = new Object();
//        Main m = (Main) o;

        // 异常的处理
        /*
        异常的捕获:
        语法格式:
        try{
            可能会抛出异常的代码;

        }catch(XxxxException e){
            解决该异常的代码;
        }
         */

        Object o = null;

        /*
        try...catch..的执行过程
        如果try代码块中没有异常, 所有的catch都不会执行.
        如果try中的某一行代码出现异常, try中的该行以后的代码都不会执行,
            检查catch有没有捕获对应的异常, 如果有, 则执行对应的catch代码块;
            如果没有, 程序直接中断.
        catch捕获异常的时候按照先后顺序依次判断, 符合多态规则.
        书写catch的时候要把子类写前面.
         */
        try {
            System.out.println("START");
            System.out.println(5 / 0);
            System.out.println(o.toString());
            System.out.println("END");
        }catch (NullPointerException e) {
            System.out.println("出现了空指针");
            System.out.println(e.getMessage());
        }  catch (RuntimeException e){
            System.out.println("捕获运行时异常");
//            throw new NullPointerException();
        } catch (Exception e){

        } finally {
            // 不管是否捕获到了异常, 该代码块都会执行
            // 一般会在该位置执行一些释放资源的操作

        }
        // try..catch..finally结构哪个部分可以省略
        // catch和finally至少得有一个

        System.out.println("测试程序是否执行完成");

        // 抛出异常  throw
        // 语法: throw 异常对象;
        // 常用于方法中, 对调用者传递的参数进行规则性的校验
        div(-1, 5);

        String s = "abcd";
//        s.charAt()

        IOTest t = new IOTest();

        try {
            t.test(5);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private static int div(int x, int y){
        // 规定 该方法只能计算两个正整数的除法
        if (x <= 0 || y <= 0){
            // NumberNotBeNegativeException
            throw new NumberNotBeNegativeException(x, y);
        }
        return x / y;
    }

}
