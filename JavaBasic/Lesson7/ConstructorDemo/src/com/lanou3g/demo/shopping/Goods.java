package com.lanou3g.demo.shopping;

public class Goods {
    public static int index = 0;
    private String name;
    private float price;

    /*
    构造方法:
    是一个特殊的方法, 没有返回值类型, 方法名与类名相同
     */
    // 默认的构造方法.
    // 如果一个类中没有构造方法, 那么会有一个隐藏的默认的构造方法
    // 创建对象的时候会调用构造方法
    // 如果类中定义了带参数的构造方法, 那么默认的构造方法就会消失
    // 如果必须要用默认的, 就显式的书写出来
    // 构造方法是可以重载的
//    public Goods(){
//
//    }
    public Goods(String name, float price) {
        this.name = name;
        this.price = price;
    }

    public void print() {
//        Goods g = new Goods("d", 5);

    }

    public static void test() {
        Goods g = new Goods("a", 5);
        g.print();
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }
}
