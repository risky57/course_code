package com.lanou3g.demo;

import com.lanou3g.demo.shopping.Goods;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // 创建对象的公式:
        // 类名 对象名 = new 类名();
        // 类名 对象名 = new 构造方法;
        Goods phone = new Goods("手机", 1999F);
        Goods clothes = new Goods("衣服", 280F);
//        phone.index = 10;
//        clothes.index = 30;
        Goods.index = 10;
        Goods.index = 30;

        System.out.println(Goods.index);

        String s1 = new String("ABC");
        String s2 = new String("ABC");

//        phone.name = "手机";
//        phone.price = 1999F;


        /*
        方法的构成:
        0个或多个修饰符 返回值类型 方法名(参数列表){
            方法体;
        }
         */
        // 1. static 修饰的方法可以直接通过类名调用
        // 2. static 修饰的变量被所有对象共享
        // 3. 在static修饰的方法中只能调用其他的static方法
        int[] array = {1, 3, 8, 4, 5, 6};
        System.out.println(Arrays.toString(array));

        // static 静态的
        /*
        当创建一个对象的时候, 实际上系统会先加载类, 然后才会
        创建对象. 类只会加载一次.
        被static修饰的属性或方法属于类, 而不属于对象.
        所以static修饰的属性或方法可以直接通过类名调用.
         */
        // 什么时候使用静态方法?
        /*
        1. 当方法中不需要引用成员变量的时候,
        这个方法可以声明为静态方法.
        2. 工具方法一般都会声明为静态的
         */
        int num = -100;
        int abs = Math.abs(num);

        int result = Math.diss(5);
        System.out.println(result);

        System.out.println(Math.fi(6));

    }
}
