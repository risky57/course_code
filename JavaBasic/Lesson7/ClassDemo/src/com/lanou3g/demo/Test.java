package com.lanou3g.demo;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {

    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        System.out.println(calendar);

        boolean b = Pattern.matches(".*\\d+.*", "abcd123eft");
        System.out.println(b);

        Pattern pattern = Pattern.compile("//d+");
        Matcher matcher = pattern.matcher("asdf92892sldf");
        boolean f = matcher.find();
        System.out.println(f);
    }

}
