package com.lanou3g.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Main {

    private static int index;

    public static void main(String[] args) {
        System.out.println(index);
        // 常用类
        // 1. 可变字符串
        // 在多线程情况下优先使用StringBuffer, 因为
        // 它是线程安全的.
        // 单线程情况下优先使用StringBuilder, 因为
        // 它的效率比StringBuffer要高
        // 用法它俩一模一样
//        StringBuffer sb = new StringBuffer();
        StringBuilder sb = new StringBuilder("初始");

        String[] strings = {"A", "B", "C", "D", "E"};
        for (int i = 0; i < strings.length; i++) {
            // 1. append 字符串的拼接
            sb.append(strings[i]);
        }
        // 3. 截取是有返回值的, 也不会对原来的造成更改
        String sub = sb.substring(2);

        // 4. 把从[start, end)部分的字符串替换为str
        sb.replace(1, 3, "倚天屠龙记");

        // 5. 把字符串翻转
        sb.reverse();

        // 6. 在某个位置插入某个值
        sb.insert(3, 2000);

        // 2. toString() 把StringBuilder对象转换为String
        String str = sb.toString();
        System.out.println(str);

        // 常用类二
        // Math 这个类提供了一些常用的数学计算方法
        // 是一个工具类
//        Math m = new Math();
//        Math.abs() 计算绝对值
        double d = Math.pow(2, 3);
        // 返回一个[0, 1)的一个随机数
        double random = Math.random();
        // 获取一个从1到100的整数
        int ran = (int)(Math.random() * 100) + 1;
        System.out.println(d);
        System.out.println(Math.PI);

        // 常用类三 时间日期相关
        long timeMillis = System.currentTimeMillis();
        System.out.println(timeMillis);
        Date date = new Date(0);
        Date date2 = new Date(timeMillis - 100000);
        System.out.println(date.after(date2));
//        date.getTime()
        // 时间格式化
        // 2018年7月10日 5:16:22
        // 2018年7月10日 5点16分22
        // year2018 month7 day 10
        // 2018-07-10 5:16:22
        SimpleDateFormat format
                = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String c = format.format(date);
        System.out.println(c);

        String f = "1988年10月25日";
        SimpleDateFormat formatter
                = new SimpleDateFormat("yyyy年MM月dd日");
        try {
            Date parse = formatter.parse(f);
            System.out.println(parse.getTime());
//            Calendar
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static void p(String str){

    }
}
