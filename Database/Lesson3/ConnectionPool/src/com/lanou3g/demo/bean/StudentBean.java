package com.lanou3g.demo.bean;

import java.sql.Date;
import java.util.List;

public class StudentBean {

    private int stuId;
    private String stuName;
    private Date birthday;
    private String stuGender;
    private List<ScoreBean> scores;

    public StudentBean() {
    }

    public StudentBean(int stuId, String stuName, Date birthday, String stuGender) {
        this.stuId = stuId;
        this.stuName = stuName;
        this.birthday = birthday;
        this.stuGender = stuGender;
    }

    @Override
    public String toString() {
        return "StudentBean{" +
                "stuId=" + stuId +
                ", stuName='" + stuName + '\'' +
                ", birthday=" + birthday +
                ", stuGender='" + stuGender + '\'' +
                '}';
    }

    public int getStuId() {
        return stuId;
    }

    public void setStuId(int stuId) {
        this.stuId = stuId;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getStuGender() {
        return stuGender;
    }

    public void setStuGender(String stuGender) {
        this.stuGender = stuGender;
    }

    public List<ScoreBean> getScores() {
        return scores;
    }

    public void setScores(List<ScoreBean> scores) {
        this.scores = scores;
    }
}
