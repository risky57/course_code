package com.lanou3g.demo.db;

import org.apache.commons.dbutils.DbUtils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * 用来获取数据库连接的
 */
public class LanouDataSource {

    private static class Holder{
        private static final LanouDataSource INSTANCE =
                new LanouDataSource();
    }

    public static LanouDataSource getInstance(){
        return Holder.INSTANCE;
    }

    private List<Connection> pool = new Vector<>();

    private String url;
    private String user;
    private String password;

    private LanouDataSource() {
        // 为了自己使用方便
        url = "jdbc:mysql://localhost:3306/new_db";
        user = "root";
        password = "root";

        try {
            for (int i = 0; i < 5; i++) {
                Connection conn = DriverManager.getConnection(url, user, password);
                pool.add(conn);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("数据库连接获取失败");
        }

    }

    public Connection getConnection() {
        if (pool.size() > 0){
            return pool.remove(0);
        }
        throw new RuntimeException("连接池中连接数量不够了");
    }

    public void release(Connection conn){
        pool.add(conn);
    }

    public void closeAll(){
        try {
            for (Connection conn : pool) {
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void loadDriver(String driverName) {
        DbUtils.loadDriver(driverName);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
