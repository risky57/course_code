package com.lanou3g.demo.common;

import com.lanou3g.demo.db.LanouDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class CustomDataSource implements ConnectionSource {
    @Override
    public Connection get() throws SQLException {
        return LanouDataSource.getInstance().getConnection();
    }
}
