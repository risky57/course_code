package com.lanou3g.demo.common;

import org.apache.commons.dbcp.BasicDataSourceFactory;

import javax.sql.DataSource;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DbcpDataSource implements ConnectionSource {

    private DataSource source;

    public DbcpDataSource() {
        try {
            Properties prop = new Properties();
            prop.load(new FileReader("src/dbcp-config.properties"));
            source = BasicDataSourceFactory.createDataSource(prop);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Connection get() throws SQLException {
        return source.getConnection();
    }
}
