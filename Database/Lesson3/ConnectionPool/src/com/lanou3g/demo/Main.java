package com.lanou3g.demo;

import com.lanou3g.demo.bean.StudentBean;
import com.lanou3g.demo.common.C3p0DataSource;
import com.lanou3g.demo.common.CustomDataSource;
import com.lanou3g.demo.common.DbcpDataSource;
import com.lanou3g.demo.db.LanouRunner;

import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception {

        // 配置连接池
//        LanouRunner.setSource(new C3p0DataSource());
//        LanouRunner.setSource(new DbcpDataSource());
        LanouRunner.setSource(new CustomDataSource());
        String sql = "select * from t_students where stu_gender = ?";
        List<StudentBean> list = LanouRunner.findAll(sql, StudentBean.class, "男");
        list.forEach(System.out::println);

        System.out.println("----------");

        String singleSql = "select * from t_students where stu_id = ?";
        StudentBean s = LanouRunner.findOne(singleSql, StudentBean.class, 5);
        System.out.println(s);





    }
}
