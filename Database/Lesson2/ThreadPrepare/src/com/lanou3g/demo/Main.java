package com.lanou3g.demo;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {


        DataSource ds =





        final BasePrinter printer = new SyncPrinter();
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    try {
                        printer.print1();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    sleep();
                }
            }
        };

        Runnable r2 = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    try {
                        printer.print2();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    sleep();
                }
            }
        };
        Runnable r3 = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    try {
                        printer.print3();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    sleep();
                }
            }
        };
        new Thread(r1).start();
        new Thread(r2).start();
        new Thread(r3).start();
    }

    public static void sleep(int time){
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void sleep(){
        sleep(1);
    }
}
