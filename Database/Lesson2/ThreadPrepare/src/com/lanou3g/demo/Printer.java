package com.lanou3g.demo;

public class Printer extends BasePrinter {

    private int flag = 1;

    public void print1() throws Exception {
        synchronized (this){
            System.out.println("富强民主文明和谐自由平等公正法治爱国敬业诚信友善" + getThreadName());
            wait();
        }
    }

    public void print2() throws Exception {
        synchronized (this){
            System.out.println("上坟烧纸放鞭炮, 引发山火罪难逃" + getThreadName());
        }
    }

    public void print3() throws Exception {
        System.out.println("让全村都怀上二胎, 是村支书不可推卸的责任" + getThreadName());
    }


}
