package com.lanou3g.demo;


public abstract class BasePrinter {
    public abstract void print1() throws Exception;
    public abstract void print2() throws Exception;
    public abstract void print3() throws Exception;

    public String getThreadName(){
        return Thread.currentThread().getName();
    }
}
