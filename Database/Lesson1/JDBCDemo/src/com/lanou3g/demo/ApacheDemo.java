package com.lanou3g.demo;

import com.lanou3g.demo.entity.Student;
import com.lanou3g.demo.utils.DBUtils;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class ApacheDemo {
    public static void main(String[] args) throws SQLException {
        DbUtils.loadDriver("com.mysql.jdbc.Driver");
//        Connection conn = DBUtils.getConnection();
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setDatabaseName("new_db");
        dataSource.setUser("root");
        dataSource.setPassword("root");
        QueryRunner runner = new QueryRunner(dataSource);
//        runner.update("delete from tb_code_type where code_type_id = ?", 100005);
        String sql = "select * from t_students";
        List<Student> students = runner.query(sql, new BeanListHandler<Student>(Student.class, new UnderlineRowProcessor()));
        System.out.println(students);
    }
}
