package com.lanou3g.demo;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;

public class UnderlineRowProcessor extends BasicRowProcessor {

    public UnderlineRowProcessor() {
        super(new GenerousBeanProcessor());
    }
}
