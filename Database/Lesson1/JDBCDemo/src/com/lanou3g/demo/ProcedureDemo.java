package com.lanou3g.demo;

import com.lanou3g.demo.utils.DBUtils;

import java.sql.*;

public class ProcedureDemo {
    public static void main(String[] args) throws SQLException {
        Connection conn = DBUtils.getConnection();
        conn.setAutoCommit(false);
        CallableStatement cs = conn.prepareCall("{call del_type(?, ?)}");
        cs.setInt(1, 100004);
        cs.registerOutParameter(2, Types.INTEGER);
        cs.executeQuery();
        int rowCount = cs.getInt(2);

        System.out.println(rowCount);
        conn.commit();
    }
}
