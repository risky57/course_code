package com.lanou3g.demo;

public class Sugar extends Coffee{
    Coffee coffee;

    public Sugar(Coffee coffee) {
        this.coffee = coffee;
    }

    public Sugar() {
    }

    @Override
    public String getName() {
        return coffee.getName() + "+糖";
    }

    @Override
    public double calculate() {
        return coffee.calculate() + 1.99;
    }
}
