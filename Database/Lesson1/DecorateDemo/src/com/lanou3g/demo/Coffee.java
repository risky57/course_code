package com.lanou3g.demo;

public abstract class Coffee {
    private String name;
    public abstract double calculate();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
