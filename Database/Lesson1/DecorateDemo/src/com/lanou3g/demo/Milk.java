package com.lanou3g.demo;

public class Milk extends Coffee {
    Coffee coffee;

    @Override
    public String getName() {
        return coffee.getName() + "+牛奶";
    }

    public Milk() {
    }

    public Milk(Coffee coffee) {
        this.coffee = coffee;
    }

    @Override
    public double calculate() {
        return coffee.calculate() +1.99;
    }
}
