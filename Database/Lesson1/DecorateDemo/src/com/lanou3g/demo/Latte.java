package com.lanou3g.demo;

public class Latte extends Coffee {
    public Latte() {
        setName("拿铁");
    }

    @Override
    public double calculate() {
        return 13.99;
    }
}
