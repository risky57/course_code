package com.lanou3g.demo;

public class Mocha extends Coffee {
    public Mocha() {
        setName("摩卡");
    }

    @Override
    public double calculate() {
        return 10.99;
    }
}
